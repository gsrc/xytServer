DROP TABLE IF EXISTS `didi_car_info`;
CREATE TABLE `didi_car_info` (
    `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
    `kid` int(11) unsigned NOT NULL DEFAULT 0 COMMENT '骑士',
    `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '手机',
    `province` varchar(5) NOT NULL DEFAULT '' COMMENT '车牌省份',
    `license_plate` varchar(10) NOT NULL DEFAULT '' COMMENT '车牌',
    `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '状态',
    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='车辆信息';

DROP TABLE IF EXISTS `didi_bill`;
CREATE TABLE `didi_bill` (
     `id` bigint(15) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
     `bill_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '账单类型：小于50是收入，否则支出',
     `object_type` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '对象类型：10用户、20商家',
     `object_id` char(36) NOT NULL DEFAULT '' COMMENT '对象id',
     `from_table` varchar(30) NOT NULL DEFAULT '' COMMENT '来源表',
     `order_sn` varchar(30) NOT NULL DEFAULT '' COMMENT '订单编号',
     `money` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
     `money_sys` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '平台金额',
     `title` varchar(30) NOT NULL DEFAULT '' COMMENT '标题',
     `content` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
     `year` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '年：2019',
     `quarter` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '季度：20191',
     `month` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '月：201905',
     `day` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '日：20190525',
     `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态：0待处理、10处理中、100已完成',
     `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
     `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账单表';
