DROP TABLE IF EXISTS `user_suggest`;
CREATE TABLE `user_suggest` (
  `uId` varchar(64) NOT NULL  COMMENT '用户id',
  `content` varchar(512) NOT NULL DEFAULT '' COMMENT '建议内容',
  `img_path` varchar (512) NOT NULL DEFAULT '' COMMENT '图片，多个以逗号分隔',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`uId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户建议表';


DROP TABLE IF EXISTS `auth_admin`;
CREATE TABLE `auth_admin` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `pid` int unsigned NOT NULL DEFAULT '0' COMMENT '上级ID',
  `account` varchar(24) NOT NULL DEFAULT '' COMMENT '账号',
  `password` varchar(256) NOT NULL DEFAULT '' COMMENT '密码',
  `phone` varchar(15) NOT NULL DEFAULT '' COMMENT '电话',
  `realName` varchar (24) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `status` tinyint unsigned NOT NULL DEFAULT '10' COMMENT '状态：0待审核、10正常、20冻结',
  `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员认证表';


/*
数据库修改方式如下
1、请在原表修改完成添加或者修改的字段。
2、再在下面完成修改的sql语句，以后线上要用。(格式如下)

add 2017.06.08 yiguidong
alter table yii2_course add `course_num` smallint DEFAULT '0' COMMENT '课程数量';
alter table yii2_course add `sort` int(11) DEFAULT '0' COMMENT '排序';

alter table yii2_course add `is_recommend` tinyint(3) unsigned DEFAULT '0' COMMENT '是否推荐：10是、0不是';
alter table yii2_admin add `is_recommend` tinyint(3) unsigned DEFAULT '0' COMMENT '是否推荐：10是、0不是';

alter table auth_user_addition add `is_manager` tinyint(3) unsigned DEFAULT '0' COMMENT '是否校园经理';
*/






























