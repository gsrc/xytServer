<?php

namespace App\Helper;

class Functions{

	public function Post($url, $curlPost){
		if (substr($url, 0, 5) == 'https'){
			return $this->post_https($url, $curlPost);
		}
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $curlPost);
		$return_str = curl_exec($curl);
		curl_close($curl);
		return $return_str;
	}

	/**
     * POST请求https接口返回内容
     * @param  string $url [请求的URL地址]
     * @param  string $post [请求的参数]
     * @return  string
     */
    private function post_https($url, $post)
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post); // Post提交的数据包
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $res = curl_exec($curl); // 执行操作
        if (curl_errno($curl)) {
            echo 'Errno'.curl_error($curl);//捕抓异常
        }
        curl_close($curl); // 关闭CURL会话
        return $res; // 返回数据，json格式
    }

	public function Get($url){
	    $curl = curl_init(); // 启动一个CURL会话
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_HEADER, 0);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    if (substr($url, 0, 5) == 'https'){
	    	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
	    	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
	    }

	    $tmpInfo = curl_exec($curl);     //返回api的json对象
	    //关闭URL请求
	    curl_close($curl);
	    return $tmpInfo;    //返回json对象
	}

    public static function isPhone($phone){
        return substr($phone,0, 1) == 1 && is_numeric($phone) && strlen($phone) == 11;
    }

    /**
     * @param $url
     * @param null $data
     * @param bool $need_json
     * @param null $header
     * @return bool|mixed|string
     */
    public static function myRequest($url, $data = null, $need_json = true, $header = null){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if(!empty($header)){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }
        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        if ($need_json) {
            return json_decode($output, true);
        }
        return $output;
    }

    public static function getOrderSn(){
        return date('YmdHis') . mt_rand(10000, 99999);
    }

    public static function randomFloat($min = 0, $max = 1) {
        $num = $min + mt_rand() / mt_getrandmax() * ($max - $min);
        return sprintf("%.2f",$num);  //控制小数后几位
    }

    public static function formatData($arr, $keys, $need_arr = false) {
	if (!is_array($keys)) {
		$keys = array($keys);
	}
	$new = array();
	foreach ($arr as $value) {
		$tmp_new = array();
		foreach ($keys as $v) {
			$tmp_new[] = $value->$v;
		}
		$tmp_new = implode( '_', $tmp_new);
        if ($need_arr) {
            $new[$tmp_new][] = $value;
        }else {
            $new[$tmp_new] = $value;
        }
	}
	return $new;
}
}


?>
