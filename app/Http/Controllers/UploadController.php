<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Image;
use Storage;
use App\Http\Models\PictureGroup;


class UploadController extends Controller
{
    //
    public function upload(Request $request){

        $types = ['images', 'word'];
        $type = 'images';
        if(in_array($request->type, $types)){
            $type = $request->type;
        }

        $file_name = uniqid() . mt_rand(100000, 999999);
        $ext = $request->file->extension();
        $folder = $type . DIRECTORY_SEPARATOR . date('Ymd');
        $path = $request->file->storeAs($folder, $file_name . '.' . $ext);
        $save_path = public_path('uploads' . DIRECTORY_SEPARATOR . $folder);
        $file_path = public_path('uploads' . DIRECTORY_SEPARATOR . $path);
        // 图片压缩处理
        // logo使用
        $img = Image::make($file_path);
        $img->resize(100, 100);
        $new = $save_path . DIRECTORY_SEPARATOR . $file_name . '_1.' . $ext;
        $img->save($new);
        // 宽屏使用
        $img = Image::make($file_path);
        $img->resize(1000, 600);
        $new = $save_path . DIRECTORY_SEPARATOR . $file_name . '_2.' . $ext;
        $img->save($new);

        $small_path = $folder . DIRECTORY_SEPARATOR . $file_name . '_1.' . $ext;
        $big_path = $folder . DIRECTORY_SEPARATOR . $file_name . '_2.' . $ext;

        $rtn['path1'] = DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR .$path;
        $rtn['path2'] = 'http://' . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $path;
        $rtn['path3'] = 'http://' . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $small_path;
        $rtn['path4'] = 'http://' . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $big_path;
        return response()->json(['code' => '0', 'msg' => 'success', 'data' => $rtn]);
    }

    public function uploadFile(Request $request){

        $base64 = $request->file;

        $ext = '';
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64, $result)){
            $ext = '.' . $result[2];
        }
        if(!isset($result[2])){
            return response()->json(['code' => '1010', 'msg' => 'file error', 'data' => []]);
        }
        $str = str_replace($result[0], '', $base64);
        $file_name = uniqid() . mt_rand(100000, 999999);
        $folder = 'images' . DIRECTORY_SEPARATOR . date('Ymd');
        $save_path = public_path('uploads' . DIRECTORY_SEPARATOR . $folder);
        $file_path = $save_path . DIRECTORY_SEPARATOR . $file_name . $ext;
        if(!file_exists($save_path)){
            mkdir($save_path, 0777);
            chmod($save_path, 0777);
        }
        $save = DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $file_name . $ext;

        if (!file_put_contents($file_path, base64_decode(($str)))){
            return response()->json(['code' => '1020', 'msg' => 'file error o', 'data' => $base64]);
        }
        $rtn['path1'] = $save;
        $rtn['path2'] = 'http://' . $_SERVER['SERVER_NAME'] . $save;
        //$rtn['path3'] = 'http://' . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $small_path;
        //$rtn['path4'] = 'http://' . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $big_path;
        return response()->json(['code' => '0', 'msg' => 'success', 'data' => $rtn]);
        /* $img = base64_decode($str);
        Header( "Content-type: image/jpeg");//直接输出显示jpg格式图片
        echo $img; */
    }

    // 添加图片
    public function addPhotos(Request $request){

        $pUrls = $request->urls;
        $pgUploadUserId = $request->uId;
        $pgScene = $request->scence;
        $pgBelongId = $request->belongId;
        $pgDescribe = $request->describe;

        if (empty($pUrls) || empty($pgUploadUserId) || empty($pgScene)){
            return $this->returnMsg(0, '缺少必要参数');
        }

        $PictureGroup = new PictureGroup();
        $PictureGroup->addPicGroup($pUrls, $pgUploadUserId, $pgScene, $pgDescribe, $pgBelongId);

        return $this->returnMsg(1, 'OK');
    }

    // 重新添加图片
    public function resetPhotos(Request $request){

        $pgUploadUserId = $request->uId;
        $pgScene = $request->scence;
        $pgBelongId = $request->belongId;
        $groupList = $request->groupList;

        $groupListArray = \json_decode($groupList, true);

        $PictureGroup = new PictureGroup();
        $PictureGroup->where('pgBelongId', $pgBelongId)->where('pgUploadUserId', $pgUploadUserId)->delete();
        $pgId = 0;
        foreach ($groupListArray as $groupInfo){
            $pgDescribe = isset($groupInfo['pgDescribe'])?$groupInfo['pgDescribe']:'';
            $pgId = $PictureGroup->addPicGroup(implode(",", $groupInfo['picture_url']), $pgUploadUserId, $pgScene, $pgDescribe, $pgBelongId);
        }

        return $this->returnMsg(1, 'OK', $pgId);
    }

    // 获取图片集列表
    public function getPhotoGroupList(Request $request){

        $pgUploadUserId = $request->uId;
        $pgBelongId = $request->belongId;

        if (empty($pgUploadUserId) || empty($pgBelongId)){
            return $this->returnMsg(0, '缺少必要参数');
        }

        $PictureGroup = new PictureGroup();
        $data = $PictureGroup->getGroupList($pgUploadUserId, $pgBelongId);

        return $this->returnMsg(1, 'OK', $data);
    }
}
