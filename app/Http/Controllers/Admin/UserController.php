<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\http\Models\School;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class UserController extends Controller
{
    /**
     * 用户列表
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        if($request->isMethod('post')){
            $model = new User();
            return $model->getList($request);
        }

        return view('admin.user.index');
    }

    /**
     * 用户提现管理
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function withdraw(Request $request){

        if($request->isMethod('post')){
            $model = new User();
            return $model->getListWithdraw($request);
        }

        return view('admin.user.withdraw');
    }

    /**
     * 同意提现
     * @param Request $request
     * @return array
     */
    public function withdrawAgree(Request $request){

        $rst = DB::table('wallet_withdraw')->where('wwId', $request->id)->update(['wwIsCheck'=>1,'wwCheckTime' => date("Y-m-d H:i:s")]);

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }

    /**
     * 设置校园经理
     * @param Request $request
     * @return array
     */
    public function setManager(Request $request){
        $authHost = config('services.extend.authHost');

        $params['id'] = $request->post('id');
        $params['uSchool'] = $request->post('uschool');

        $url = $authHost . "/auth/setManager";

        $result = https_request($url, $params, true);

        return ['code' => '0', 'msg' => '成功', 'data' => $result];
    }

    /**
     * 拒绝提现
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function withdrawRefuse(Request $request){

        if($request->isMethod('post')){
            DB::table('wallet_withdraw')->where('wwId', $request->id)->update(['wwIsCheck'=>2, 'wwRefuseReason'=>$request->wwRefuseReason,'wwCheckTime' => date("Y-m-d H:i:s")]);

            return ['code' => '0', 'msg' => '成功', 'data' => []];
        }

        return view('admin.user.withdrawRefuse',['id'=> $request->id]);
    }

    /**
     * 用户信息
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userinfo(Request $request){


        // 查询用户信息
        $authHost = config('services.extend.authHost');
        $user = https_request($authHost . "/auth/userList", ['ids' => $request->id], true);
        if(!$user){
            exit("用户不存在了");
        }
        $data = json_decode($user[0]['uCustomInfo'], true);
        $data['csName'] = '';
        if(isset($data['csId'])){
            $data['csName'] = School::find($data['csId'])->csName;
            unset($data['csId']);
        }
        //var_dump($data);exit;

        return view('admin.user.userinfo', ['data' => $data]);
    }

	public function add(Request $request){
		
		$School = new School();
		if($request->isMethod('post')){
			
			$uCode = $request->uCode;
			$csId = $request->csId;
			$data = array();
			$data['uCustomInfo'] = '{"csId":'.$csId.'}';
			$schoolInfo = $School->find($csId);
			$data['uSchool'] = $schoolInfo->csName;
			
			$model = new User();
			$result = $model->setUserAddition($uCode, $data);

			return ['code' => '0', 'msg' => '成功', 'data' => []];
        }
        
        $uCode = $request->uCode;

        $model = new User();
        $userInfo = $model->getUserInfoFromCode($uCode);

		
		$schoolList = $School->getAllSchoolList();
		$schoolList = json_decode(json_encode($schoolList), true);

        return view('admin.user.add', ['userInfo'=> $userInfo, 'schoolList' => $schoolList]);
	}
}
