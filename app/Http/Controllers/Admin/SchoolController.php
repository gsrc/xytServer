<?php

namespace App\Http\Controllers\Admin;

use App\Models\ConfigRegion;
use App\Models\SchoolSite;
use App\Models\SchoolTake;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\http\Models\School;

class SchoolController extends Controller
{
    /**
     * 学校首页
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        if($request->isMethod('post')){
            $model = new School();
            return $model->getList($request);
        }

        return view('admin.school.index');
    }

    /**
     * 配送点
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function distribute(Request $request){

        if($request->isMethod('post')){
            $model = new SchoolTake();
            return $model->getList($request);
        }

        return view('admin.school.distribute',['id'=>$request->id, 'request' => $request]);
    }

    /**
     * 配送点添加
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function distributeAdd(Request $request){

        if($request->isMethod('post')){
            if($request->stId == ''){
                $rst = DB::table('school_take')->insert(['stId'=>$request->stId,'stName'=>$request->stName]);
            }else{
                $rst = DB::table('school_take')->where('stId', $request->stId)->update(['stName'=>$request->stName]);
            }
            return ['code' => '0', 'msg' => '成功', 'data' => $rst];
        }

        $stName = '';
        if($request->stId != ''){
            $stName = DB::table('school_take')->where('stId', $request->stId)->value("stName");
        }

        return view('admin.school.distributeAdd',['request'=>$request,'stName'=>$stName]);
    }

    /**
     * 配送点删除
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function distributeDel(Request $request){

        $rst = DB::table('school_take')->where('stId', $request->id)->delete();

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }

    /**
     * 学校添加
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request){
        if($request->isMethod('post')){
            if($request->csId != ''){
                School::where('csId', $request->csId)->update(['crId' => $request->crId, 'csName' => $request->csName]);
            }else{
                $model = new School();
                $model->crId = $request->crId;
                $model->csName = $request->csName;
                $model->save();
            }

            return ['code' => '0', 'msg' => '成功', 'data' => ''];
        }

        // 获取城市
        $city = ConfigRegion::getCity();
        // 修改时
        $school = [];
        if($request->id != '') {
            $school = School::where('csId', $request->id)->first();
        }

        $rtn['city'] = $city;
        $rtn['school']  = $school;
        return view('admin.school.add', $rtn);
    }

    /**
     * 站点列表
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function site(Request $request){

        if($request->isMethod('post')){
            $model = new SchoolSite();
            return $model->getList($request);
        }

        $title_add = config("config.school_site_type.en." . $request->type);

        return view('admin.school.site',['request' => $request, 'title_add' => $title_add]);
    }

    /**
     * 站点添加
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function siteAdd(Request $request){

        if($request->isMethod('post')){
            if($request->ssId == ''){
                $rst = DB::table('school_site')->insert(['ssType' => $request->ssType, 'csId'=>$request->csId,'ssName'=>$request->ssName]);
            }else{
                $rst = DB::table('school_site')->where('ssId', $request->ssId)->update(['ssName'=>$request->ssName]);
            }
            return ['code' => '0', 'msg' => '成功', 'data' => $rst];
        }

        $ssName = '';
        if($request->ssId != ''){
            $ssName = DB::table('school_site')->where('ssId', $request->ssId)->value("ssName");
        }

        $title_add = config("config.school_site_type.en." . $request->type);

        return view('admin.school.siteAdd',['request'=>$request,'ssName'=>$ssName, 'title_add' => $title_add]);
    }

    /**
     * 站点删除
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function siteDel(Request $request){

        $rst = DB::table('school_site')->where('ssId', $request->id)->delete();

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }
}
