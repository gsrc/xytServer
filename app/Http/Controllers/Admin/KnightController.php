<?php

namespace App\Http\Controllers\Admin;

use App\http\Models\Knight;
use App\http\Models\KnightApply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class KnightController extends Controller
{
    /**
     * 骑士列表
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        if($request->isMethod('post')){
            $model = new Knight();
            return $model->getList($request);
        }

        return view('admin.knight.index');
    }

    /**
     * 申请列表
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function applyList(Request $request){

        if($request->isMethod('post')){
            $model = new KnightApply();
            return $model->getList($request);
        }

        return view('admin.knight.applyList');
    }

    /**
     * 申请验证通过
     * @param Request $request
     * @return array
     */
    public function verify(Request $request){

        $rst = DB::table('knight_apply')->where('kaId', $request->id)->update(['kaStatus'=>1,'kaCheckUser'=>session('admin.realName'),'kaCheckTime' => date("Y-m-d H:i:s")]);

        // 数据添加到骑士表
        $rst = DB::select("insert into knight(uId,csId,klId,kStatus,kCheckTime)
 select uId,csId,klId,kaStatus,kaCheckTime  
 from knight_apply where kaId = ?", [$request->id]);

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }

    /**
     * 申请拒绝
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function refuse(Request $request){

        if($request->isMethod('post')){
            DB::table('knight_apply')->where('kaId', $request->id)->update(['kaStatus'=>2,'kaCheckUser'=>session('admin.realName'),'kaRefuseReason'=>$request->kaRefuseReason,'kaCheckTime' => date("Y-m-d H:i:s")]);

            return ['code' => '0', 'msg' => '成功', 'data' => []];
        }

        return view('admin.knight.refuse',['id'=> $request->id]);
    }

    /**
     * 自动任务设置
     * @param Request $request
     * @return array
     */
    public function autoTask(Request $request){

        $rst = DB::table('knight')->where('kId', $request->id)->update(['kAutoTask' => $request->status]);

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }
}
