<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class StatisticsController extends Controller
{
    /**
     * 用户列表
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        $x = [];
        $y = [];
        $c = [];
        $pid = [];
        $sum = 0;
        $amount = 0;
        $status = Order::getStatus();

        foreach ($status as $k => $v){
            $tmp['name'] = $v;
            $tmp['value'] = 0;
            $pie[] = $tmp;
        }
        if(!empty($request->time_start) || !empty($request->time_end)){
            $start = strtotime($request->time_start);
            $end = strtotime($request->time_end . ' 23:59:59');
            // 天的信息
            $d1 =  date("d", $start);
            $d2 =  date("d", $end);
            // 月的信息
            $m1 =  date("m", $start);
            $m2 =  date("m", $end);
            // 年的信息
            $y1 =  date("Y", $start);
            $y2 =  date("Y", $end);
            DB::enableQueryLog();
            $model = DB::table('agency_order');
            $model->where('aoStatus', '>', '0');// 付款订单
            if($y1 == $y2 && $m1 == $m2 && $d1 == $d2){// 如果同天就查看一天内时间段的数据
                $date = date("Y-m-d ", $start);
                // 处理x轴数据
                $x = ['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '24:00'];
                $x_data[] = [strtotime($date . '00:00:00'), strtotime($date . '04:00:00')];
                $x_data[] = [strtotime($date . '04:00:00'), strtotime($date . '08:00:00')];
                $x_data[] = [strtotime($date . '08:00:00'), strtotime($date . '12:00:00')];
                $x_data[] = [strtotime($date . '12:00:00'), strtotime($date . '16:00:00')];
                $x_data[] = [strtotime($date . '16:00:00'), strtotime($date . '20:00:00')];
                $x_data[] = [strtotime($date . '20:00:00'), strtotime($date . '23:59:59')];

            }else if($y1 == $y2 && $m1 == $m2 && $d1 != $d2){// 如果同月就查看一月内时间段的数据
                // 处理x轴数据
                $d_num = $d2 - $d1 + 1;
                for($i = 0;$i < $d_num;$i++){
                    $x[] = date('Y-m-d', $start + $i * 86400);
                    $x_data[] = [strtotime(date('Y-m-d 00:00:00', $start + $i * 86400)), strtotime(date('Y-m-d 23:59:59', $start + $i * 86400))];
                }
            }else if($y1 == $y2 && $m1 != $m2){// 如果月不同就查看几个月的数据
                // 处理x轴数据
                $m_num = $m2 - $m1 + 1;
                for($i = 0;$i < $m_num;$i++){
                    $x[] = date('Y-m', strtotime(date('Y-m-d', $start) . "+$i month"));// 月数
                    $f_day = date('Y-m-01 00:00:00', strtotime(date('Y-m-d', $start) . "+$i month"));// 每月第一天
                    $date = date('Y-m', strtotime(date('Y-m-d', $start) . "+$i month"));
                    $l_day = date('Y-m-d 23:59:59', strtotime("$date +1 month -1 day"));// 每月最后一天
                    $x_data[] = [strtotime($f_day), strtotime($l_day)];
                }
            }else{
                exit('暂不支持多年的数据查询');
            }
            $model->where('createTime', '>', date('Y-m-d 00:00:00',$start))->where('createTime', '<=', date("Y-m-d 23:59:59",$end));

            $order = $model->get();//dd($order);
            $order2 = $model->select(DB::raw('count(*) as num, aoStatus'))->groupBy("aoStatus")->get();
            //print_r(DB::getQueryLog());exit;
            // 根据x轴重组数据
            if($order){
                foreach($x_data as $k => $v){
                    $price = 0;
                    $count = 0;
                    foreach($order as $v1){//echo $v[0] . "=" . $v1->createTime. "="  . $v[1];exit;
                        $t1 = $v[0];
                        $t2 = strtotime($v1->createTime);
                        $t3 = $v[1];
                        if($t1 <= $t2 && $t2 <= $t3){
                            $price += $v1->aoPay;
                            $count++;
                        }
                    }
                    $y[$k] = $price;
                    $sum += $price;
                    $c[$k] = $count;
                    $amount += $count;
                }
            }

            if($order2){
                $pie = [];

                foreach ($status as $k => $v){
                    $tmp['name'] = $v;
                    $tmp['value'] = 0;
                    foreach ($order2 as $v2){
                        if($k == $v2->aoStatus){
                            $tmp['value'] = $v2->num;
                        }
                    }
                    $pie[] = $tmp;
                }

            }

        }

        $rtn['query'] = ['time' => 1];
        $rtn['request'] = $request;
        $rtn['x'] = json_encode($x);
        $rtn['y1'] = json_encode($c);
        $title = "单量统计【{$amount}】，";
        $rtn['y2'] = json_encode($y);
        $title = $title . "收益统计【{$sum}】";
        $rtn['title'] = $title;
        $rtn['status'] = json_encode(Order::getStatus(true));
        $rtn['pie'] = json_encode($pie);
        //dd($rtn);
        return view('admin.statistics.index', $rtn);
    }
	
	/**
     * 用户列表
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function user(Request $request){

        $x = [];
        $y = [];
        $c = [];
        $pid = [];
        $sum = 0;
        $amount = 0;
        $status = Order::getStatus();

        foreach ($status as $k => $v){
            $tmp['name'] = $v;
            $tmp['value'] = 0;
            $pie[] = $tmp;
        }
        if(!empty($request->time_start) || !empty($request->time_end)){
            $start = strtotime($request->time_start);
            $end = strtotime($request->time_end . ' 23:59:59');
            // 天的信息
            $d1 =  date("d", $start);
            $d2 =  date("d", $end);
            // 月的信息
            $m1 =  date("m", $start);
            $m2 =  date("m", $end);
            // 年的信息
            $y1 =  date("Y", $start);
            $y2 =  date("Y", $end);
            DB::enableQueryLog();
            $model = DB::table('agency_order');
            $model->where('aoStatus', '>', '0');// 付款订单
            if($y1 == $y2 && $m1 == $m2 && $d1 == $d2){// 如果同天就查看一天内时间段的数据
                $date = date("Y-m-d ", $start);
                // 处理x轴数据
                $x = ['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '24:00'];
                $x_data[] = [strtotime($date . '00:00:00'), strtotime($date . '04:00:00')];
                $x_data[] = [strtotime($date . '04:00:00'), strtotime($date . '08:00:00')];
                $x_data[] = [strtotime($date . '08:00:00'), strtotime($date . '12:00:00')];
                $x_data[] = [strtotime($date . '12:00:00'), strtotime($date . '16:00:00')];
                $x_data[] = [strtotime($date . '16:00:00'), strtotime($date . '20:00:00')];
                $x_data[] = [strtotime($date . '20:00:00'), strtotime($date . '23:59:59')];

            }else if($y1 == $y2 && $m1 == $m2 && $d1 != $d2){// 如果同月就查看一月内时间段的数据
                // 处理x轴数据
                $d_num = $d2 - $d1 + 1;
                for($i = 0;$i < $d_num;$i++){
                    $x[] = date('Y-m-d', $start + $i * 86400);
                    $x_data[] = [strtotime(date('Y-m-d 00:00:00', $start + $i * 86400)), strtotime(date('Y-m-d 23:59:59', $start + $i * 86400))];
                }
            }else if($y1 == $y2 && $m1 != $m2){// 如果月不同就查看几个月的数据
                // 处理x轴数据
                $m_num = $m2 - $m1 + 1;
                for($i = 0;$i < $m_num;$i++){
                    $x[] = date('Y-m', strtotime(date('Y-m-d', $start) . "+$i month"));// 月数
                    $f_day = date('Y-m-01 00:00:00', strtotime(date('Y-m-d', $start) . "+$i month"));// 每月第一天
                    $date = date('Y-m', strtotime(date('Y-m-d', $start) . "+$i month"));
                    $l_day = date('Y-m-d 23:59:59', strtotime("$date +1 month -1 day"));// 每月最后一天
                    $x_data[] = [strtotime($f_day), strtotime($l_day)];
                }
            }else{
                exit('暂不支持多年的数据查询');
            }
            $model->where('createTime', '>', date('Y-m-d 00:00:00',$start))->where('createTime', '<=', date("Y-m-d 23:59:59",$end));

            $order = $model->get();//dd($order);
            $order2 = $model->select(DB::raw('count(*) as num, aoStatus'))->groupBy("aoStatus")->get();
            //print_r(DB::getQueryLog());exit;
            // 根据x轴重组数据
            if($order){
                foreach($x_data as $k => $v){
                    $price = 0;
                    $count = 0;
                    foreach($order as $v1){//echo $v[0] . "=" . $v1->createTime. "="  . $v[1];exit;
                        $t1 = $v[0];
                        $t2 = strtotime($v1->createTime);
                        $t3 = $v[1];
                        if($t1 <= $t2 && $t2 <= $t3){
                            $price += $v1->aoPay;
                            $count++;
                        }
                    }
                    $y[$k] = $price;
                    $sum += $price;
                    $c[$k] = $count;
                    $amount += $count;
                }
            }

            if($order2){
                $pie = [];

                foreach ($status as $k => $v){
                    $tmp['name'] = $v;
                    $tmp['value'] = 0;
                    foreach ($order2 as $v2){
                        if($k == $v2->aoStatus){
                            $tmp['value'] = $v2->num;
                        }
                    }
                    $pie[] = $tmp;
                }

            }

        }

        $rtn['query'] = ['time' => 1];
        $rtn['request'] = $request;
        $rtn['x'] = json_encode($x);
        $rtn['y1'] = json_encode($c);
        $title = "单量统计【{$amount}】，";
        $rtn['y2'] = json_encode($y);
        $title = $title . "收益统计【{$sum}】";
        $rtn['title'] = $title;
        $rtn['status'] = json_encode(Order::getStatus(true));
        $rtn['pie'] = json_encode($pie);
        //dd($rtn);
        return view('admin.statistics.index', $rtn);
    }

    /**
     * 用户提现管理
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function withdraw(Request $request){

        if($request->isMethod('post')){
            $model = new User();
            return $model->getListWithdraw($request);
        }

        return view('admin.user.withdraw');
    }

    /**
     * 同意提现
     * @param Request $request
     * @return array
     */
    public function withdrawAgree(Request $request){

        $rst = DB::table('wallet_withdraw')->where('wwId', $request->id)->update(['wwIsCheck'=>1,'wwCheckTime' => date("Y-m-d H:i:s")]);

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }

    /**
     * 拒绝提现
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function withdrawRefuse(Request $request){

        if($request->isMethod('post')){
            DB::table('wallet_withdraw')->where('wwId', $request->id)->update(['wwIsCheck'=>2, 'wwRefuseReason'=>$request->wwRefuseReason,'wwCheckTime' => date("Y-m-d H:i:s")]);

            return ['code' => '0', 'msg' => '成功', 'data' => []];
        }

        return view('admin.user.withdrawRefuse',['id'=> $request->id]);
    }

}
