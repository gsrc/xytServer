<?php

namespace App\Http\Controllers\Admin;

use App\Models\ConfigRegion;
use App\Models\Express;
use App\Models\SchoolSite;
use App\Models\SchoolTake;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\http\Models\School;

class ExpressController extends Controller
{
    /**
     * 快递管理
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){

        if($request->isMethod('post')){
            $model = new Express();
            return $model->getList($request);
        }

        return view('admin.express.index');
    }

    /**
     * 删除
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function del(Request $request){

        $rst = DB::table('config_express')->where('ceId', $request->id)->delete();

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }

    /**
     * 添加
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request){
        if($request->isMethod('post')){
            if($request->ceId != ''){
                Express::where('ceId', $request->ceId)->update(['ceName' => $request->ceName, 'ceSort' => $request->ceSort]);
            }else{
                $model = new Express();
                $model->ceName = $request->ceName;
                $model->ceSort = $request->ceSort;
                $model->save();
            }

            return ['code' => '0', 'msg' => '成功', 'data' => ''];
        }

        // 修改时
        $data = [];
        if($request->id != '') {
            $data = Express::where('ceId', $request->id)->first();
        }

        $rtn['data']  = $data;
        return view('admin.express.add', $rtn);
    }

    /**
     * 站点列表
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function site(Request $request){

        if($request->isMethod('post')){
            $model = new SchoolSite();
            return $model->getList($request);
        }

        $title_add = config("config.school_site_type.en." . $request->type);

        return view('admin.school.site',['request' => $request, 'title_add' => $title_add]);
    }

    /**
     * 站点添加
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function siteAdd(Request $request){

        if($request->isMethod('post')){
            if($request->ssId == ''){
                $rst = DB::table('school_site')->insert(['ssType' => $request->ssType, 'csId'=>$request->csId,'ssName'=>$request->ssName]);
            }else{
                $rst = DB::table('school_site')->where('ssId', $request->ssId)->update(['ssName'=>$request->ssName]);
            }
            return ['code' => '0', 'msg' => '成功', 'data' => $rst];
        }

        $ssName = '';
        if($request->ssId != ''){
            $ssName = DB::table('school_site')->where('ssId', $request->ssId)->value("ssName");
        }

        $title_add = config("config.school_site_type.en." . $request->type);

        return view('admin.school.siteAdd',['request'=>$request,'ssName'=>$ssName, 'title_add' => $title_add]);
    }

    /**
     * 站点删除
     * @param Request $request
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function siteDel(Request $request){

        $rst = DB::table('school_site')->where('ssId', $request->id)->delete();

        return ['code' => '0', 'msg' => '成功', 'data' => $rst];
    }
}
