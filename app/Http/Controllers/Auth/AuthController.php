<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helper\Functions;
use App\Http\Models\AgencyOrder;

class AuthController extends Controller
{
	// 请求扩展数据
	public function extendRequest($requestPath, $requestData=null){
		$Functions = new Functions();
		$authHost = config('services.extend.authHost');
		$requestUrl = $authHost.$requestPath;

		if (empty($requestData)){
			$data = $Functions->Get($requestUrl);
		}else{
			$data = $Functions->Post($requestUrl, $requestData);
		}
		return $data;
	}
}
