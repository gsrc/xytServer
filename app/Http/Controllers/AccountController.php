<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function admin(Request $request)
    {
        //$encrypted = encrypt("123123");
        $admin = DB::table('auth_admin')->where('account', $request->uname)->first();
        if(!$admin){
            return ['code' => '1010', 'msg' => '账号密码错误', 'data' => []];
        }

        $decrypted = decrypt($admin->password);

        if($request->password != $decrypted){
            return ['code' => '1010', 'msg' => '账号密码错误了', 'data' => []];
        }

        session(['admin.id' => $admin->id]);
        session(['admin.realName' => $admin->realName]);

        return ['code' => '0', 'msg' => '登录成功', 'data' => []];
    }

    public function shopBindWechat($shopAccount, $openid){

        $shopInfo = DB::connection('mysqlShop')->table('all_shop')->where('account', $shopAccount)->first();

        if (!empty($shopInfo)){
            $bindInfo = DB::connection('mysqlShop')->table('all_shop_binding')->where('sid', $shopInfo->id)->where('openid', $openid)->first();
            if (empty($bindInfo)){
                DB::connection('mysqlShop')->table('all_shop_binding')->insert([
                    'sid' => $shopInfo->id, 'openid' => $openid, 'nickname' => $shopAccount
                ]);
                return 'ok:' . $shopInfo->shop_name . '订单通知服务绑定成功';
            }else{
                return 'ok:' . $shopInfo->shop_name . '订单通知服务绑定成功';
            }
        }else{
            Log::info($shopAccount);
            if (strlen($shopAccount) == 11){
                $phone = $shopAccount;
                $shopInfo = DB::connection('mysqlShop')
                                ->table('all_shop')
                                ->select('all_shop.*')
                                ->leftJoin('all_shop_binding', 'all_shop.id', 'all_shop_binding.sid')
                                ->where('all_shop.phone', $phone)
                                ->where('all_shop_binding.openid', $openid)
                                ->first();
                Log::info('商家绑定付款微信公众号：');
                Log::info(\json_encode($shopInfo));
                if (!empty($shopInfo)){
                    $bindInfoList = DB::connection('mysqlShop')->table('all_shop_binding')->where('sid', $shopInfo->id)->where('type', 3)->get();

                    foreach($bindInfoList as $bindInfo){
                        if ($bindInfo->openid != $openid){
                            DB::connection('mysqlShop')->table('all_shop_binding')->where('id', $bindInfo->id)->delete();
                        }
                    }

                    DB::connection('mysqlShop')
                        ->table('all_shop_binding')
                        ->where('sid', $shopInfo->id)
                        ->where('openid', $openid)
                        ->update(['type'=>3, 'realname'=>$shopAccount]);
                    return 'ok:' . $shopInfo->shop_name . '收款服务绑定成功';
                }
            }
        }

        return 'ng';
    }

    public function bindALLKnightForWechar($openid, $schoolId, $phone){

        $authHost = config('services.extend.authHost');
        $commonId = config('services.extend.commonId');
        $postData = [];
        $postData['uMobile'] = $phone;
        $postData['uChannelId'] = $commonId;
        $userRtnStr = https_request($authHost . "/auth/getUserInfoFromUserMobile", $postData);
        $userRtnObj = \json_decode($userRtnStr, true);
        if ($userRtnObj['code'] == 0){
            return 'ng';
        }

        $userInfo = $userRtnObj['data'];
        $userCode = $userInfo['uCode'];

        $knightInfo = DB::table('knight')->where('uId', $userCode)->where('csId', $schoolId)->where('kStatus', 1)->first();
        if (empty($knightInfo)){
            return 'ng';
        }

        $bindInfo = DB::connection('mysqlShop')->table('all_shop_binding')->where('openid', $openid)->where('school_id', $schoolId)->where('is_knight', 1)->first();
        if (empty($bindInfo)){
            DB::connection('mysqlShop')->table('all_shop_binding')->insert([
                'openid' => $openid, 'school_id' => $schoolId, 'is_knight' => 1
            ]);
        }

        return 'ok';
    }
}
