<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class CommentController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * 评论添加
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){
        $uId = 1;
        if(empty($request->uChannel)){
            return response()->json(['code' => '0', 'msg' => '请设置渠道', 'data' => []]);
        }
        $insert['uChannel'] = $request->uChannel;
        $originType = [10,20,30,40];
        if(!in_array($request->originType,$originType)){
            return response()->json(['code' => '0', 'msg' => '请设置正确的来源类型', 'data' => []]);
        }
        $insert['originType'] = $request->originType;
//        if(empty($request->content)){
//            //return response()->json(['code' => '0', 'msg' => '请设置评论类容', 'data' => []]);
//        }
        if(empty($request->originId)){
            return response()->json(['code' => '0', 'msg' => '请设置评论对象', 'data' => []]);
        }
        $insert['originId'] = $request->originId;

        if(empty($request->tagId)){
            return response()->json(['code' => '0', 'msg' => '请设置评论标签', 'data' => []]);
        }

        // 获取标签数据
        $tag = DB::table('user_comment_tag')->where('uChannel',$request->uChannel)->where('tagId',$request->tagId)->first();
        if(!$tag){
            return response()->json(['code' => '0', 'msg' => '该标签不存在', 'data' => []]);
        }
        $insert['uId'] = $uId;
        $insert['content'] = $request->input('content');
        if(!empty($request->commentId)){ // 下级评论
            // 获取上级评论的根id
            $rId = DB::table('user_comment')->where('commentId', $request->commentId)->value('rId');
            $insert['pId'] = $request->commentId;
            $insert['rId'] = $rId;
            $id = DB::table('user_comment')->insertGetId($insert);
        }else{
            $id = DB::table('user_comment')->insertGetId($insert);
            if(!$id){
                return response()->json(['code' => '1010', 'msg' => '添加失败', 'data' => []]);
            }
            DB::table('user_comment')->where('commentId', $id)->update(['rId' => $id]);
        }
        // 添加评论标签信息
        $record['commentId'] = $id;
        $record['tagId'] = $request->tagId;
        $record['tagName'] = $tag->tagName;
        $record['tagScore'] = $tag->tagScore;
        DB::table('user_comment_tag_record')->insert($record);
        return response()->json(['code' => '0', 'msg' => '添加成功', 'data' => []]);
    }

    /**
     * 评论点赞
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function like(Request $request){

        return response()->json(['code' => '0', 'msg' => '添加成功', 'data' => []]);
    }

    /**
     * 评论列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function commentlist(Request $request){

        return response()->json(['code' => '0', 'msg' => '添加成功', 'data' => []]);
    }

    /**
     * 标签列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function taglist(Request $request){
        if(empty($request->uChannel)){
            return response()->json(['code' => '0', 'msg' => '请设置渠道', 'data' => []]);
        }
        $data = DB::table('user_comment_tag')->where('uChannel', $request->uChannel)->get();

        return response()->json(['code' => '0', 'msg' => '添加成功', 'data' => $data]);
    }

}
