<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\http\Models\User;

class AccountController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * 管理员登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function admin(Request $request){

        $user = new User();
        // 验证登录
        $user = $user->verifyLogin($request->uAccount, $request->uPassword);
        if ($user){
            $this->addLoginLog($user->uId, $request->ip, 'management');

            return response()->json(['code' => '1010', 'msg' => '用户不存在', 'data' => []]);
        } else {
            return response()->json(['code' => '1010', 'msg' => '用户不存在', 'data' => []]);
        }
    }

    /**
     * 用户登录
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request){
        if(!isset($request->uAccount)){
            return response()->json(['code' => '1010', 'msg' => '用户不存在', 'data' => []]);
        }

        $user = new User();
        // 验证登录
        $user = $user->verifyLogin($request->uAccount, $request->uPassword);
        if ($user){
            $this->addLoginLog($user->uId, $request->ip, 'management');

            return response()->json(['code' => '1010 ', 'msg' => '用户不存在', 'data' => []]);
        } else {
            return response()->json(['code' => '1010', 'msg' => '用户不存在', 'data' => []]);
        }
    }
}
