<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;

class ExpressController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * 快递发布
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(Request $request){
        // 用户验证
        $user = 1;
        $insert['uId'] = $user;
        if(empty($request->ceId)){
            return response()->json(['code' => '1010', 'msg' => '请设置地址ID', 'data' => []]);
        }
        $insert['ceId'] = $request->ceId;

        if(empty($request->aoExpressWeight)){
            return response()->json(['code' => '1010', 'msg' => '请设置快递大小', 'data' => []]);
        }
        $insert['aoExpressWeight'] = $request->aoExpressWeight;

        if(empty($request->aoLevel)){
            return response()->json(['code' => '1010', 'msg' => '请设置快递紧急级别', 'data' => []]);
        }
        $insert['aoLevel'] = $request->aoLevel;

        if(empty($request->aoPay)){
            return response()->json(['code' => '1010', 'msg' => '请设置付款金额', 'data' => []]);
        }
        $insert['aoPay'] = $request->aoPay;

        if(empty($request->aoSmsCopy)){
            return response()->json(['code' => '1010', 'msg' => '请设置快递短信', 'data' => []]);
        }
        $insert['aoSmsCopy'] = $request->aoSmsCopy;

        if(!empty($request->aoMemo)){
            $insert['aoMemo'] = $request->aoMemo;
            //return response()->json(['code' => '1010', 'msg' => '请设置备注', 'data' => []]);
        }

        $rst = DB::table('agency_order')->insert($insert);
        if(!$rst){
            return response()->json(['code' => '1010', 'msg' => '提交失败', 'data' => []]);
        }

        return response()->json(['code' => '1010', 'msg' => '提交成功', 'data' => []]);
    }

    

}
