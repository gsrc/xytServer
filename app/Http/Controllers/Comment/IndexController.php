<?php

namespace App\Http\Controllers\Comment;

use App\Helper\Functions;
use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Models\Comment;

class IndexController extends Controller
{
    // 添加评论
    public function addComment(Request $request){

        $token = $request->token;
        $userInfo = $this->getUserInfoFromToken($token, true);

        $cPid = $request->cPid;
        $userId = $userInfo['uCode'];
        $csId = $userInfo['addition']['uCustomInfo']['csId'];
        $cContent = $request->cContent;
        $pgId = $request->pgId;

        if (!$cPid){
            $cPid = 0;
        }

        $Comment = new Comment();
        $Comment->addComment($csId, $cPid, $userId, $cContent, $pgId);

        return $this->returnMsg(1, 'OK');
    }

    public function getCommentList(Request $request){

        $token = $request->token;
        $userInfo = $this->getUserInfoFromToken($token, true);
        $userId = $userInfo['uCode'];
        $page = $request->page;

        if (empty($page)){
            $page = 1;
        }

        $csId = $userInfo['addition']['uCustomInfo']['csId'];

        $Comment = new Comment();
        $dataList = $Comment->getCommentList($csId, $page, $userId);
        
        return $this->returnMsg(1, 'OK', $dataList);
    }
}
