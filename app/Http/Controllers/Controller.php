<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Helper\Functions;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $COMMON_ID;

    function __construct(){
        $this->COMMON_ID = config('services.extend.commonId');
    }

    // 返回json信息
    public function returnMsg($code, $msg, $data=false){

        $return['code'] = $code;
        $return['msg'] = $msg;
        if ($data !== false){
            $return['data'] = $data;
        }

        return json_encode($return, true);
    }

    // 验证token并获取用户信息
    public function getUserInfoFromToken($token, $isWithAddition=true){

        $uChannelId = $this->COMMON_ID;

        $authHost = config('services.extend.authHost');
        $tail = '';
        if ($isWithAddition){
            $tail = '&withAddition=1';
        }
        $getInfoUrl = $authHost.'/auth/getUserInfoFromToken?token='.$token.$tail;

        $Functions = new Functions();
        $userInfo = $Functions->Get($getInfoUrl);
        $userInfo = json_decode($userInfo, true);

        if ($userInfo['code'] == 0){
            echo json_encode($userInfo);
            exit;
        }

        $returnInfo = $userInfo['data'];
        return $returnInfo;
    }

    // 通过用户id获取用户信息
    public function getUserInfoFromUid($uId, $isWithAddition=true){

        $uChannelId = $this->COMMON_ID;

        $authHost = config('services.extend.authHost');
        $data = array();
        $data['uCode'] = $uId;
        $data['uChannelId'] = $uChannelId;

        if ($isWithAddition){
            $data['withAddition'] = 1;
        }
        $getInfoUrl = $authHost.'/auth/getUserInfoFromUserCode';

        $Functions = new Functions();
        $userInfo = $Functions->Post($getInfoUrl, $data);
        $userInfo = json_decode($userInfo, true);

        if ($userInfo['code'] == 0){
            echo json_encode($userInfo);
            exit;
        }

        $returnInfo = $userInfo['data'];
        return $returnInfo;
    }

    // 通过用户id获取用户信息
    public function getUserInfoFromOpenid($openid, $isWithAddition=true){

        $uChannelId = $this->COMMON_ID;

        $authHost = config('services.extend.authHost');
        $data = array();
        $data['uThirdId'] = $openid;
        $data['uChannelId'] = $uChannelId;

        if ($isWithAddition){
            $data['withAddition'] = 1;
        }
        $getInfoUrl = $authHost.'/auth/getUserInfoFromOpenid';

        $Functions = new Functions();
        $userInfo = $Functions->Post($getInfoUrl, $data);
        $userInfo = json_decode($userInfo, true);

        if ($userInfo['code'] == 0){
            echo json_encode($userInfo);
            exit;
        }

        $returnInfo = $userInfo['data'];
        return $returnInfo;
    }

    // 通过用户id获取用户信息
    public function getUserInfoFromUserMobile($uMobile){

        $uChannelId = $this->COMMON_ID;

        $authHost = config('services.extend.authHost');
        $data = array();
        $data['uMobile'] = $uMobile;
        $data['uChannelId'] = $uChannelId;

        $getInfoUrl = $authHost.'/auth/getUserInfoFromUserMobile';

        $Functions = new Functions();
        $userInfo = $Functions->Post($getInfoUrl, $data);
        $userInfo = json_decode($userInfo, true);

        if ($userInfo['code'] == 0){
            echo json_encode($userInfo);
            exit;
        }

        $returnInfo = $userInfo['data'];
        return $returnInfo;
    }

//        DB::connection()->enableQueryLog();#开启执行日志
//      print_r(DB::getQueryLog());   //获取查询语句、参数和执行时间
}
