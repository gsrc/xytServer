<?php

namespace App\Http\Controllers\Didi;

use App\Helper\Functions;
use App\Models\Didi\Bill;
use App\Models\Didi\CarInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Http\Models\AgencyAddress;
use App\Http\Models\AgencyOrder;
use App\Http\Models\Knight;
use App\http\Models\KnightWork;
use App\http\Models\UserFree;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Auth\IndexController as authIndex;

class UserController extends Controller
{
    public $userInfo = null;
    public $uid = null;

    public function __construct(Request $request)
    {
        if (empty($request->token)) {
            return $this->returnMsg(1010, '请设置token');
        }
        $this->userInfo = $this->getUserInfoFromToken($request->token);
        $this->uid = $this->userInfo['uCode'];
    }

    /**
     * 校验邀请码
     * @param Request $request
     * @return false|string
     */
    public function checkInviteCode(Request $request){
        if (empty($request->code)) {
            return $this->returnMsg(1010, '请设置邀请码');
        }
        $url = config('services.extend.shopHost') . "/api/open/oil/checkInviteCode";
        $rst = Functions::myRequest($url, ['code' => $request->code]);
        if ($rst['code'] != 0) {
            return $this->returnMsg(1010, '邀请码错误' , $rst);
        }
        // 是否被邀请过
        $rst2 = json_decode($this->beInvited($request), true);
        if ($rst2['code'] != 0) {
            return $this->returnMsg(1010, $rst2['msg']);
        }
        $rst = Knight::query()->where('uId', $this->uid)
            ->where('cardShopId', 0)
            ->update(['cardShopId' => $rst['data']['id']]);


        return $this->returnMsg(0, '验证成功');
    }

    /**
     * 是否可以被邀请
     * @param Request $request
     * @return false|string
     */
    public function beInvited(Request $request){
        $rst = Knight::query()->where('uId', $this->uid)->first();
        if (empty($rst)) {
            Knight::query()->insert(['uId' => $this->uid]);
            $rst = Knight::query()->where('uId', $this->uid)->first();
        }
        if ($rst->cardShopId > 0) {
            return $this->returnMsg(1020, '您已经被邀请过了');
        }
        return $this->returnMsg(0, '可以被邀请');
    }

    /**
     * 车牌设置
     * @param Request $request
     * @return false|string
     */
    public function carSet(Request $request){
        // 邀请过才能设置信息
        $rst = json_decode($this->beInvited($request), true);
        if ($rst['code'] == 0) {
            return $this->returnMsg(1010, "您没有被邀请过，不能注册");
        }
        $input = $request->all(['phone', 'license_plate', 'username']);
        if (!Functions::isPhone($input['phone'])) {
            return $this->returnMsg(1010, '请填写正确的手机号');
        }
        $input['province'] = mb_substr($input['license_plate'], 0, 1);
        $rst = CarInfo::query()->where('uid', $this->uid)->first();
        if ($rst) {
            $rst = CarInfo::query()->where('uid', $this->uid)->update($input);
        } else {
            $input['uid'] = $this->uid;
            $rst = CarInfo::query()->insert($input);
        }

        return $this->returnMsg(0, 'OK', $rst);
    }

    public function carInfo(Request $request){

        $rst = CarInfo::query()->where('uid', $this->uid)->first();
        return $this->returnMsg(0, 'OK', $rst);
    }

    /**
     * 账单列表
     * @param Request $request
     * @return false|string
     */
    public function billList(Request $request){
        $rst = Bill::query()
            ->where('object_type', Bill::$object_type['didi_user'])
            ->where('object_id', $this->uid)->first();
        if (empty($rst)) {
            // 初始化2个月的账单
            Bill::initBill($this->uid);
        }
        $page = $request->input('page', 1);
        $start = $page-1;
        $offset = $start * 2000;
        $rst = Bill::query()->where('object_id', $this->uid)
            ->where('object_type', Bill::$object_type['didi_user'])
            ->orderBy('order_sn', 'desc')
            ->offset($offset)
            ->limit(2000)
            ->get();
        $rst = Functions::formatData($rst, 'day', true);
        $list = [];
        foreach ($rst as $k => $v) {
            $tmp['day'] = substr($k, 0, 4) . '-' . substr($k, 4, 2) .
                '-' . substr($k, 6, 2);
            $tmp['list'] = $v;
            $list[] = $tmp;
        }

        $rtn['money'] = CarInfo::query()->where('uid', $this->uid)->value('money');
        $rtn['list'] = $list;
        return $this->returnMsg(1, 'ok', $rtn);
    }
}
