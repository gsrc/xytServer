<?php
namespace App\http\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

Class KnightWork extends Model
{
	
	protected $table = 'knight_work';
	
	 //指定主键
    protected $primaryKey = 'kwId';

	//自动维护时间戳
    public $timestamps = false;
	
	// 添加派任务日志
	public function addAutoInfo($kId){
		$today = date("Y-m-d");
		$now = date('Y-m-d H:i:s', time());
		$workInfo = $this::where('kId', $kId)
						->where('kwDay', $today)
						->first();
		if ($workInfo){
			$workInfo->kwAssignNum = $workInfo->kwAssignNum + 1;
			$workInfo->kwLastAssignTime = $now;
			$workInfo->save();
			return;
		}
		
		$KnightWork = new KnightWork();
		$KnightWork->kId = $kId;
		$KnightWork->kwDay = $today;
		$KnightWork->kwLastAssignTime = $now;
		$KnightWork->kwAssignNum = 1;
		
		$KnightWork->save();
		return;
	}
	
	public function addDoneNum($kId){
		$today = date("Y-m-d");
		
		$workInfo = $this::where('kId', $kId)
						->where('kwDay', $today)
						->first();
		
		if ($workInfo){
			$workInfo->kwDoneNum = $workInfo->kwDoneNum + 1;
			$workInfo->save();
			return;
		}
		
		$KnightWork = new KnightWork();
		$KnightWork->kId = $kId;
		$KnightWork->kwDay = $today;
		$KnightWork->kwAssignNum = 0;
		$KnightWork->kwDoneNum = 1;
		
		$KnightWork->save();
		return;
	}
}
?>