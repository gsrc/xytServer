<?php
namespace App\http\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

Class KnightApply extends Model
{
	
	protected $table = 'knight_apply';
	
	 //指定主键
    protected $primaryKey = 'kaId';

	//自动维护时间戳
    public $timestamps = false;
	
	// 获取报名信息
	public function getInfoByUid($uId){
		
		$applyInfo = $this::where('uId', $uId)->orderBy('kaId', 'DESC')->first();
		return $applyInfo;
	}

	// 添加报名信息
	public function addInfo($uId, $csId, $klId){
		
		$reflection = new \ReflectionMethod($this, __FUNCTION__);
		$parameters = $reflection->getParameters();
		$paramValues = func_get_args();
		
		$KnightApply = new KnightApply();
		for ($i=0;$i<count($parameters);$i++){
			$KnightApply[$parameters[$i]->name] = $paramValues[$i];
		}
		$KnightApply->save();
		return $KnightApply;
	}
	
	// 获取申请信息
	public function getCurrentApplyInfo($uId){
		
		$info = $this::where('uId', $uId)->orderBy('kaId', 'DESC')->first();
		
		return $info;
	}

    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table($this->table);

        $ids = [];
        if($request->key != ''){
            // 查询用户信息
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['key' => $request->key], true);
            if($user){
                $ids = get_ids($user, 'uCode', true);
            }
        }
        if($ids){
            $query->whereIn('uid', $ids);
        }

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('kaStatus', 'asc')->orderBy('createTime', 'desc')->get();
        if($data){
            // 获取用户信息
            $ids = get_ids($data, 'uId');
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['ids' => implode(',', $ids)], true);
            $new_data2 = data_format($user, 'uCode', true);
            // 获取学校信息
            $ids = get_ids($data, 'csId');
            $school = DB::table('config_school')->whereIn('csId', $ids)->get();
            $new_data = data_format($school, 'csId');
            // 获取等级信息
            $ids = get_ids($data, 'klId');
            $level = DB::table('knight_level')->whereIn('klId', $ids)->get();
            $new_data3 = data_format($level, 'klId');
            foreach ($data as $k => $v){
                $data[$k]->uSex = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uSex'] : '';
                if($data[$k]->uSex == 1){
                    $data[$k]->uSex = '男';
                }else{
                    $data[$k]->uSex = '女';
                }
                $data[$k]->uMobile = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uMobile'] : '';
                $data[$k]->uName = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uName'] : '';
                $data[$k]->csName = isset($new_data[$v->csId]) ? $new_data[$v->csId]->csName : '';
                $data[$k]->klName = isset($new_data3[$v->klId]) ? $new_data3[$v->klId]->klName : '';
            }
        }

        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }
}
?>