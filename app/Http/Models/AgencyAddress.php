<?php
namespace App\http\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

Class AgencyAddress extends Model
{
    
    protected $table = 'agency_address';
    
     //指定主键
    protected $primaryKey = 'aaId';

    //自动维护时间戳
    public $timestamps = false;
    
    public $outColumn = [];
    
    // 添加地址
    public function add($uId, $aaName, $aaSex, $aaMobile, $csId, $csName, $ssId, $ssName, $aaAddress, $aaCustomAddress=''){
        $aaDefault = 0;
        $info = $this::where('uId', $uId)->first();
        if (empty($info)){
            $aaDefault = 1;
        }
        
//        print_r(__FUNCTION__);exit;
        $reflection = new \ReflectionMethod($this, __FUNCTION__);
        $parameters = $reflection->getParameters();
        $paramValues = func_get_args();
        $AgencyAddress = new AgencyAddress();
        for ($i=0;$i<count($parameters);$i++){
            $AgencyAddress[$parameters[$i]->name] = $paramValues[$i];
        }
        $AgencyAddress['aaDefault'] = $aaDefault;
        if ($aaCustomAddress){
            $AgencyAddress['aaType'] = 'custom';
        }
        $AgencyAddress->save();
    }
    
    // 获取地址列表
    public function getList($uId, $aaType='experss'){
        $list = $this::where('uId', $uId)->where('aaType', $aaType)->get();
        return $list;
    }
    
    // 获取默认地址
    public function getDefault($uId){
        $addressInfo = $this::where('uId', $uId)->where('aaDefault', 1)->first();
        return $addressInfo;
    }
    
    // 获取地址
    public function getInfo($aaId, $uId){
        $addressInfo = $this::where('aaId', $aaId)->where('uId', $uId)->first();
        return $addressInfo;
    }
    
    // 删除地址
    public function delAddress($aaId, $uId){
        $info = $this::find($aaId);
        if (empty($info)){
            return '地址不存在';
        }
        if ($info->uId == $uId){
            $info->delete();
            return true;
        }else{
            return '不是合法用户';
        }
    }
}
?>