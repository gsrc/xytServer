<?php
namespace App\http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

Class School extends Model
{
	
	protected $table = 'config_school';
	
	 //指定主键
    protected $primaryKey = 'csId';

	//自动维护时间戳
    public $timestamps = false;

    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table($this->table);

        if($request->key != ''){
            $query->where('csName','like','%'.$request->key.'%');
        }

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('createTime', 'desc')->get();
        if($data){
            // 获取城市信息
            $ids = get_ids($data, 'crId');
            $level = DB::table('config_region')->whereIn('crId', $ids)->get();
            $new_data = data_format($level, 'crId');
            foreach ($data as $k => $v){
                $data[$k]->crName = isset($new_data[$v->crId]) ? $new_data[$v->crId]->crName : '';
                $data[$k]->url_edit = url('/admin/school/add', ['id' => $v->csId, 'name' => $v->csName]);
                $data[$k]->url_sushe = url('/admin/school/site', ['csId' => $v->csId,'type'=>config("config.school_site_type.de.sushe"), 'name' => $v->csName]);
                $data[$k]->url_peisongdian = url('/admin/school/site', ['csId' => $v->csId,'type'=>config("config.school_site_type.de.peisongdian"), 'name' => $v->csName]);
            }
        }

        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }
    
    public function getAllSchoolList(){
    	
    	$data = $this::select(['csId', 'csName'])->where('csEnable', 1)->get();
    	
    	return $data;
    }
}
?>