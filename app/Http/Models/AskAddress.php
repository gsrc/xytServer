<?php
namespace App\http\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

Class AskAddress extends Model
{
	
	protected $table = 'knight_ask_address';
	
	 //指定主键
    protected $primaryKey = 'kId';

	//自动维护时间戳
    public $timestamps = false;
	


}
?>