<?php
namespace App\http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\http\Models\PictureGroup;
use App\Http\Controllers\Controller;

Class Comment extends Model
{

    protected $table = 'comment';

     //指定主键
    protected $primaryKey = 'cId';

    //自动维护时间戳
    public $timestamps = false;

    // 添加评论
    public function addComment($csId, $cPid, $userId, $cContent, $pgId){

        $reflection = new \ReflectionMethod($this, __FUNCTION__);
        $parameters = $reflection->getParameters();
        $paramValues = func_get_args();

        $Comment = new Comment();
        for ($i=0;$i<count($parameters);$i++){
            $Comment[$parameters[$i]->name] = $paramValues[$i];
        }

        $Comment->save();
        return $Comment;
    }

    // 获取列表
    public function getCommentList($csId, $page, $userId){

        $limit = 20;
        $offset = ($page-1)*$limit;

        $Comment = new Comment();
        $cList = $Comment->where('csId', $csId)
                    ->where('cPid', 0)
                    ->orderBy('cId', 'DESC')
                    ->offset($offset)
                    ->limit($limit)
                    ->get();

        $PictureGroup = new PictureGroup();
        $Controller = new Controller();
        for($i=0;$i<count($cList);$i++){
            $replyList = $Comment->where('cPid', $cList[$i]->cId)->get();
            for($j=0;$j<count($replyList);$j++){
                $userInfo = $Controller->getUserInfoFromUid($replyList[$j]->userId, false);
                $replyList[$j]['userInfo'] = $userInfo;
            }
            $cList[$i]->replyList = $replyList;
            $cList[$i]->userInfo = $Controller->getUserInfoFromUid($cList[$i]->userId, false);
            $picList = DB::table('picture_url')->where('pgId', $cList[$i]->pgId)->pluck('pUrl');
            $cList[$i]->picList = $picList;
        }

        return $cList;
    }


}
?>
