<?php
namespace App\http\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

Class UserFree extends Model
{
	
	protected $table = 'user_free';
	
	 //指定主键
    protected $primaryKey = 'ufId';

	//自动维护时间戳
    public $timestamps = false;
	


}
?>