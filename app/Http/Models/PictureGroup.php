<?php

namespace App\http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class PictureGroup extends Model
{
    protected $table = 'picture_group';
    //指定主键
    protected $primaryKey = 'pgId';
    //自动维护时间戳
    public $timestamps = false;

    public function addPicGroup($urls, $pgUploadUserId, $pgScene, $pgDescribe, $pgBelongId){

        $urlArray = explode(",", $urls);

        $pgId = DB::table($this->table)->insertGetId(
            ['pgUploadUserId' => $pgUploadUserId, 'pgScene' => $pgScene, 'pgDescribe' => $pgDescribe, 'pgBelongId' => $pgBelongId]
        );

        foreach ($urlArray as $pUrl){
            DB::table('picture_url')->insert([
                'pgId' => $pgId, 'pUrl' => $pUrl
            ]);
        }

        return $pgId;
    }

    public function getGroupList($pgUploadUserId, $pgBelongId){

        $groupList = DB::table($this->table)->where('pgUploadUserId', $pgUploadUserId)->where('pgBelongId', $pgBelongId)->get();
        for($i=0;$i<count($groupList);$i++){
            $urls = DB::table('picture_url')->where('pgId', $groupList[$i]->pgId)->pluck('pUrl');
            $groupList[$i]->picture_url = $urls;
        }

        return $groupList;
    }
}
