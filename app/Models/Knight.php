<?php
namespace App\http\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\http\Models\KnightWork;


Class Knight extends Model
{
    
    protected $table = 'knight';
    
     //指定主键
    protected $primaryKey = 'kId';

    //自动维护时间戳
    public $timestamps = false;
    
    // 获取骑士信息
    public function getInfoByUid($uId){
        
        $knightInfo = $this::where('uId', $uId)
                        ->where('kStatus', 1)
                        ->first();
        return $knightInfo;
    }
    
    // 设置骑士信息
    public function setInfo($kId, $kAutoTask=null){
        
        $reflection = new \ReflectionMethod($this, __FUNCTION__);
        $parameters = $reflection->getParameters();
        $paramValues = func_get_args();

        $knight = $this->find($kId);
        for ($i=0;$i<count($parameters);$i++){
            // 单独处理0
            if (!empty($paramValues[$i]) || ($paramValues[$i] === 0 || $paramValues[$i] === '0')){
                $knight[$parameters[$i]->name] = $paramValues[$i];
            }
        }

        $knight->save();
        return $knight;
    }
    
    /**
     * 获取指定送货地址
     */
    public function AskAddress()
    {
        return $this->hasMany('App\http\Models\AskAddress', 'kId', 'kId');
    }

    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table($this->table);

        $ids = [];
        if($request->key != ''){
            // 查询用户信息
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['key' => $request->key], true);
            if($user){
                $ids = get_ids($user, 'uCode', true);
            }
        }
        if($ids){
            $query->whereIn('uid', $ids);
        }

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('createTime', 'desc')->get();
        if($data){
            // 获取用户信息
            $ids = get_ids($data, 'uId');
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['ids' => implode(',', $ids)], true);
            $new_data2 = data_format($user, 'uCode', true);
            // 获取学校信息
            $ids = get_ids($data, 'csId');
            $school = DB::table('config_school')->whereIn('csId', $ids)->get();
            $new_data = data_format($school, 'csId');
            // 获取等级信息
            $ids = get_ids($data, 'klId');
            $level = DB::table('knight_level')->whereIn('klId', $ids)->get();
            $new_data3 = data_format($level, 'klId');
            foreach ($data as $k => $v){
                $data[$k]->uName = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uName'] : '';
                $data[$k]->csName = isset($new_data[$v->csId]) ? $new_data[$v->csId]->csName : '';
                $data[$k]->klName = isset($new_data3[$v->klId]) ? $new_data3[$v->klId]->klName : '';
                $data[$k]->uMobile = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uMobile'] : '';
            }
        }

        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }

    // 添加骑士
    public function addKnight($uId, $csId, $klId){
        
        $reflection = new \ReflectionMethod($this, __FUNCTION__);
        $parameters = $reflection->getParameters();
        $paramValues = func_get_args();
        
        $Knight = new Knight();
        for ($i=0;$i<count($parameters);$i++){
            $Knight[$parameters[$i]->name] = $paramValues[$i];
        }

        $Knight->save();
        return $Knight;
    }
    
    // 设置骑士工作状态
    public function setKnightWorkStatus($kId, $doingStatus){
        
        $today = date("Y-m-d");
        if ($doingStatus === 1){
            DB::table('knight')->where('kId', $kId)->increment('kIsWorking');
        }else if ($doingStatus === 3){
            DB::table('knight')->where('kId', $kId)->decrement('kIsWorking');
        }
    }
    
    // 添加接订单数
    public function addGetNum($kId){
        
        $this::find($kId)->increment('kGetNum');
    }
    
    // 添加完成订单数
    public function addDoneNum($kId){
        
        $this::find($kId)->increment('kDoneNum');
        
        $KnightWork = new KnightWork();
        $KnightWork->addDoneNum($kId);
    }
    
    // 获取在线骑士人数
    public function getOnlineList($csId){
        
        $list = $this::where('csId', $csId)
                ->where('kStatus', 1)
                ->where('kAutoTask', 1)
                ->where('csId', $csId)
                ->get();        

        return $list;
    }
    
    // 获取骑士列表
    public function getKnightList(){
        
    }
}
?>