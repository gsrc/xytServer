<?php

namespace App\Models\Didi;

use App\Helper\Functions;
use DemeterChain\B;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bill extends Model
{
    protected $table = 'didi_bill';

    public static $object_type = [
        'didi_user' => 10,
    ];

    public static $bill_type = [
        'didi_income' => 10,
    ];

    public static function initBill($uid){
        $money = 0;
        $arr = [];
        for ($i = 0; $i < 20; $i++) {
            $num = mt_rand(15, 35);
//            $num = mt_rand(2, 4);
            for ($j = 0; $j < $num; $j++) {
                $insert['bill_type'] = Bill::$bill_type['didi_income'];
                $insert['object_type'] = Bill::$object_type['didi_user'];
                $insert['object_id'] = $uid;
//                $insert['order_sn'] = Functions::getOrderSn();
                $insert['money'] = Functions::randomFloat(9, 40);
                if ($insert['money'] < 11) {
                    $insert['money'] = 9;
                }
                $money += $insert['money'];
                $insert['title'] = '收入';
                $insert['content'] = '快车接单入账';
                $insert['status'] = 100;
                $insert['year'] = date("Y");
                $day = date("Y-m-d",strtotime("-{$i} day"));
                $day2 = date("Y-m-d H:i:s",strtotime("-{$i} day"));
                $insert['order_sn'] = str_replace(['-', ' ', ':'], '', $day2) . mt_rand(10000, 99999);
                $insert['month'] = date("Ym", strtotime($day));
                $insert['day'] = str_replace("-", '', $day);
                $quarter = ceil(date('n') / 3);
                $insert['quarter'] = $quarter;
                $arr[] = $insert;
            }
        }
        Bill::query()->insert($arr);
        CarInfo::query()->where('uid', $uid)->update(['money' => $money]);
    }


}
