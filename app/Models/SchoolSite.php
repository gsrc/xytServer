<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class SchoolSite extends Model
{
    protected $table = 'school_site';

    //指定主键
    protected $primaryKey = 'ssId';

    //自动维护时间戳
    public $timestamps = false;

    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table($this->table);

        if($request->key != ''){
            $query->where('ssName','like','%'.$request->key.'%');
        }

        $query->where('csId', $request->csId);
        $query->where('ssType', $request->type);

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('createTime', 'desc')->get();
        if($data){
            foreach ($data as $k => $v){
                $data[$k]->url_edit = url('/admin/school/siteAdd', ['csId' => $v->csId,'type' => $v->ssType, 'ssId' => $v->ssId]);
                $data[$k]->url_del = url('/admin/school/siteDel', ['id' => $v->ssId]);
            }
        }

        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }
}
