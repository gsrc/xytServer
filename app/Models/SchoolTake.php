<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class SchoolTake extends Model
{
    protected $table = 'school_take';

    //指定主键
    protected $primaryKey = 'stId';

    //自动维护时间戳
    public $timestamps = false;

    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table($this->table);

        if($request->key != ''){
            $query->where('stName','like','%'.$request->key.'%');
        }

        $query->where('csId', $request->id);

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('createTime', 'desc')->get();
        if($data){
            foreach ($data as $k => $v){
                $data[$k]->url_edit = url('/admin/school/distributeAdd', ['id' => $v->csId  , 'stId' => $v->stId]);
                $data[$k]->url_del = url('/admin/school/distributeDel', ['id' => $v->stId]);
            }
        }

        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }
}
