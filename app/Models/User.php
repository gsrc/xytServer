<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class User extends Model
{

    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);

        $params['page'] = $page;
        $params['limit'] = $page_num;
        $params['key'] = $request->key;
        $params['is_manager'] = $request->is_manager;
        $authHost = config('services.extend.authHost');
        $user = https_request($authHost . "/auth/userPage", $params, true);
        if($user && $user['data']){
            foreach ($user['data'] as $k => $v) {
                if($v['uSex'] == 1){
                    $user['data'][$k]['uSex'] = '男';
                }else{
                    $user['data'][$k]['uSex'] = '女';
                }
            }
        }

        return $user;
    }

    /**
     * 提现列表
     * @return array
     */
    public function getListWithdraw($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table("wallet_withdraw");

        $ids = [];
        if($request->key != ''){
            // 查询用户信息
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['key' => $request->key], true);
            if($user){
                $ids = get_ids($user, 'uCode', true);
            }
        }
        if($ids){
            $query->whereIn('uid', $ids);
        }

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('wwIsCheck', 'asc')->orderBy('createTime', 'desc')->get();
        if(!$data->isEmpty()){
            // 获取用户信息
            $ids = get_ids($data, 'uId');
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['ids' => implode(',', $ids)], true);
            $new_data2 = data_format($user, 'uCode', true);

            foreach ($data as $k => $v){
                $data[$k]->uName = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uName'] : '';
            }
        }

        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }
    
    // 获取用户信息
    public function getUserInfoFromCode($uCode){
    	
    	$authHost = config('services.extend.authHost');
    	$commonId = config('services.extend.commonId');
    	
    	$params = array();
    	$params['uCode'] = $uCode;
    	$params['uChannelId'] = $commonId;
    	$params['withAddition'] = 1;
    	
    	$url = $authHost . "/auth/getUserInfoFromUserCode";

        $userInfo = https_request($url, $params, true);
        
        if (isset($userInfo['code'])){
        	if ($userInfo['code'] == 0){
        		return false;
        	}
        }else{
        	return false;
        }
        
        $userInfo = $userInfo['data'];
        return $userInfo;
    }
    
    //设置用户附加信息
    public function setUserAddition($uCode, $params){
    	
    	$authHost = config('services.extend.authHost');
    	$commonId = config('services.extend.commonId');
    	
    	$params['uCode'] = $uCode;
    	$params['uChannelId'] = $commonId;
    	
    	$url = $authHost . "/auth/setUserAddition";
    	
    	$result = https_request($url, $params, true);
    	
    	return $result;
    }
}
