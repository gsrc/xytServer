<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Express extends Model
{
    protected $table = 'config_express';

    //指定主键
    protected $primaryKey = 'ceId';
    //自动维护时间戳
    public $timestamps = false;
    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table($this->table);

        if($request->key != ''){
            $query->where('ceName','like','%'.$request->key.'%');
        }

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('ceSort', 'desc')->get();
        if($data){
            foreach ($data as $k => $v){
                $data[$k]->url_edit = url('/admin/express/add', ['id' => $v->ceId]);
                $data[$k]->url_del = url('/admin/express/del', ['id' => $v->ceId]);
            }
        }

        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }

}
