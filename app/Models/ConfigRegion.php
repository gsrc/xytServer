<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigRegion extends Model
{
    protected $table = 'config_region';

    //指定主键
    protected $primaryKey = 'crId';

    public static function getCity(){
        return self::where('crType', 2)->get();
    }
}
