<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Order extends Model
{
    protected $table = 'agency_order';

    //指定主键
    protected $primaryKey = 'aoId';

    /**
     * 列表查询
     * @return array
     */
    public function getList($request){

        $page = $request->input('page', 1);
        $page_num = $request->input('limit', 15);;
        $start = ($page - 1) * $page_num;
        $query = DB::table($this->table);

        $ids = [];
        if($request->key != ''){
            // 查询用户信息
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['key' => $request->key], true);
            if($user){
                $ids = get_ids($user, 'uCode', true);
            }
        }
        if($ids){
            $query->whereIn('uid', $ids);
        }

        $count = $query->count();
        $data = $query->offset($start)->limit($page_num)->orderBy('createTime', 'desc')->get();
        if($data){
            // 获取用户信息
            $ids = get_ids($data, 'uId');
            $authHost = config('services.extend.authHost');
            $user = https_request($authHost . "/auth/userList", ['ids' => implode(',', $ids)], true);
            $new_data2 = data_format($user, 'uCode', true);
            // 获取学校信息
            $ids = get_ids($data, 'csId');
            $school = DB::table('config_school')->whereIn('csId', $ids)->get();
            $new_data = data_format($school, 'csId');
            // 获取骑手
            //$ids = get_ids($data, 'kId');
            //$knight = DB::table('knight')->whereIn('kId', $ids)->get();
            $ids = get_ids($data, 'kUid');
            $authHost = config('services.extend.authHost');
            $user2 = https_request($authHost . "/auth/userList", ['ids' => implode(',', $ids)], true);
            $new_data4 = data_format($user2, 'uCode', true);

//            // 获取等级信息
//            $ids = get_ids($data, 'klId');
//            $level = DB::table('knight_level')->whereIn('klId', $ids)->get();
//            $new_data3 = data_format($level, 'klId');
            foreach ($data as $k => $v){
                $data[$k]->uName = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uName'] : '';
                $data[$k]->uName2 = isset($new_data4[$v->kUid]) ? $new_data4[$v->kUid]['uName'] . ',' . $new_data4[$v->kUid]['uMobile'] : '';
                $data[$k]->uMobile = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uMobile'] : '';
                $data[$k]->csName = isset($new_data[$v->csId]) ? $new_data[$v->csId]->csName : '';
                $data[$k]->time_cancel = self::getTimeDiff($v->aoCancelTime, $v->createTime);
                $data[$k]->time_accept = self::getTimeDiff($v->aoAcceptTime, $v->createTime);
//                $data[$k]->klName = isset($new_data3[$v->klId]) ? $new_data3[$v->klId]->klName : '';
//                $data[$k]->uMobile = isset($new_data2[$v->uId]) ? $new_data2[$v->uId]['uMobile'] : '';
            }
        }


        return ['code' => '0', 'msg' => '成功', 'count' => $count, 'data' => $data];
    }

    // 处理时间差
    public static function getTimeDiff($end , $start){
        $time = '';
        if(!empty($end)){
            $tmp = (strtotime($end) - strtotime($start))/60;
            if($tmp > 60){
                $time = round($tmp/60, 1);
                return "-" . $time . "时";
            }else{
                $time = round($tmp, 0);
                return "-" . $time . "分";
            }
        }
        return $time;
    }

    public static function getStatus($is_title = false){
        //0发布中；1被接取；2已拿到快递；3已完成；4已评价；-1用户取消；-2未支付；-3已派单给骑士
        if($is_title){
            return ['发布中','被接取','已拿到快递','已完成','已评价','用户取消','未支付','已派单给骑士'];
        }
        return [
            '0' => '发布中',
            '1' => '被接取',
            '2' => '已拿到快递',
            '3' => '已完成',
            '4' => '已评价',
            '-1' => '用户取消',
            '-2' => '未支付',
            '-3' => '已派单给骑士',
        ];
    }
}
