<?php

return [

    'school_site_type' => [
        'en' => [
			'10' => '宿舍',
			'20' => '配送点'
		],
		'de' => [
			'sushe' => '10',
			'peisongdian' => '20'
		]
    ],

];
