<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>larryms框架之Echarts扩展组件</title>
    <meta name="keywords" content="swiper" />
    <meta name="description" content="swiper" />
    <meta name="Author" content="larry" />
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="Shortcut Icon" href="{{ URL::asset('favicon.ico') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/layui/css/layui.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/base.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/animate.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/library.css') }}" media="all">
</head>

<body class="larryms-layout-bgB box-blank  library-box">
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <div class="layui-fluid margin15">
        <div class="layui-row layui-col-space15">

            <div class="layui-col-lg12 layui-col-md12 layui-col-sm12 layui-col-xs12">
                <section class="layui-card">
                    <div class="layui-card-body" style="padding: 15px;">
                        <blockquote class="layui-elem-quote quoteBox" id="articleBtn">
                            <div class="layui-show-xs layui-hide-sm layui-hide-md layui-hide-lg" style="padding-bottom: 12px;">
                                <div class="layui-inline">
                                    <a class="layui-btn layui-btn-normal addNews_btn layui-btn-sm" style="margin-right: 10px;"  data-type="add" data-url="admin/school/add" data-group="0" data-id="27" data-icon="larry-fabu1">发布文章</a>
                                </div>
                                <div class="layui-inline">
                                    <a class="layui-btn layui-btn-danger layui-btn-normal delAll_btn  layui-btn-sm"  data-type="del" data-id="article">批量删除</a>
                                </div>
                            </div>
                            <form action="{{url('/admin/statistics/index')}}" method="get" class="layui-form  layui-inline">
                                <div class="larryms-search-box">
                                    <div class="layui-inline">
                                        <div class="layui-input-inline">
                                            <input type="text" name="time_start" id="time_start" placeholder="开始时间" value="{{$request->time_start}}" autocomplete="off" class="layui-input">
                                        </div>
                                    </div>
                                    <div class="layui-inline">
                                        <div class="layui-input-inline">
                                            <input type="text" name="time_end" id="time_end" placeholder="结束时间" value="{{$request->time_end}}" autocomplete="off" class="layui-input">
                                        </div>
                                    </div>
                                    <input type="submit" class="layui-btn " lay-submit lay-filter="formDemo" value="查询">
                                </div>
                            </form>
                            <div class="layui-hide-xs layui-inline">
                                <div style="font-size:12px;color:red;text-align:center;">备注:暂时不支持跨年查询。①月份不同则按月查询。②月份相同日期不同则按日查询。③日期相同则查询当日</div>

                                {{--<div class="layui-inline">--}}
                                    {{--<a class="layui-btn layui-btn-normal addNews_btn"  data-type="add" data-url="{{url('/admin/school/add')}}" data-group="0" data-id="121" data-icon="larry-fabu1">添加学校</a>--}}
                                {{--</div>--}}
                                {{--<div class="layui-inline">--}}
                                {{--<a class="layui-btn layui-btn-danger layui-btn-normal delAll_btn"  data-type="del" data-id="article">批量删除</a>--}}
                                {{--</div>--}}
                            </div>

                        </blockquote>
                    </div>
                </section>
            </div>
        </div>
        <div class="layui-row  layui-col-space15 larryms-echarts">

            <div class="layui-col-lg12 layui-col-md12 layui-col-sm12 layui-col-xs12">
                 <section class="layui-card">
                    <div class="layui-card-header">
                        单量与金额
                    </div>
                    <div class="layui-card-body">
                        <div class="larryms-charts-box" style="width: 100%;height: 500px;" id="demo1"></div>
                    </div>
                </section>
            </div>
        </div>


        <div class="layui-row  layui-col-space15 larryms-echarts">
            <div class="layui-col-lg12 layui-col-md12 layui-col-sm12 layui-col-xs12">
                <section class="layui-card">
                    <div class="layui-card-header">
                        各状态数据统计
                    </div>
                    <div class="layui-card-body">
                        <div class="larryms-charts-box" style="width: 100%;height: 500px;" id="demo5"></div>
                    </div>
                </section>
            </div>

        </div>

    </div>
<div id="config" data-title1="{{$title}}" data-x="{{$x}}" data-y1="{{$y1}}" data-y2="{{$y2}}" data-status="{{$status}}" data-pie="{{$pie}}"></div>
    <!-- 加载js文件-->
<script type="text/javascript" src="{{ URL::asset('assets/larryms/layui/layui.js') }}"></script>
    <script type="text/javascript">
    layui.config({
        version: "2.0.8",
        base: '/assets/larryms/',
        page: 'statistics',
        identified: 'echarts'
    }).extend({
        larry: '/js/base'
    }).use(['larry','form','laydate'],function(){
        var $ = layui.$,
        laydate = layui.laydate;

        laydate.render({
            elem:'#time_start',
            value: new Date(),
            isInitValue: false,
            type: 'date',
            format: 'yyyy-MM-dd'
        });

        laydate.render({
            elem:'#time_end',
            value: new Date(),
            isInitValue: false,
            type: 'date',
            format: 'yyyy-MM-dd'
        });


    });
    </script>
</body>

</html>