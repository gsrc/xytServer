<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>后台首页</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="Author" content="larry" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="Shortcut Icon" href="{{ URL::asset('favicon.ico') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/layui/css/layui.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/base.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/console.css') }}" media="all">
</head>

<body class="larry-bg-gray">
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <h1 style="margin-top: 200px;margin-left: 200px;">欢迎来到校运通管理平台！</h1>
    <!-- 加载js文件-->
<script type="text/javascript" src="{{ URL::asset('assets/larryms/layui/layui.js') }}"></script>
    <script type="text/javascript">
    layui.cache.page = 'console';
    layui.config({
        version: "2.0.8",
        base: '/assets/larryms/',
        identified: 'main'
    }).extend({
        larry: '/js/base'
    }).use('larry');
    </script>
</body>

</html>