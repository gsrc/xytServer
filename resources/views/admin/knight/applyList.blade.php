<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>所有文章列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="Author" content="larry" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="Shortcut Icon" href="{{ URL::asset('favicon.ico') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/layui/css/layui.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/base.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/admin.css') }}" media="all">
</head>

<body class="">
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <div class="layui-fluid">
        <div class="larry-container">
            <div class="layui-row layui-col-space15 larryms-data-top">
                <div class="layui-col-xs12 layui-col-sm12 layui-col-md12 layui-col-lg12">
                    <blockquote class="layui-elem-quote quoteBox" id="articleBtn">
                        <div class="layui-show-xs layui-hide-sm layui-hide-md layui-hide-lg" style="padding-bottom: 12px;">
                            <div class="layui-inline">
                                <a class="layui-btn layui-btn-normal addNews_btn layui-btn-sm" style="margin-right: 10px;"  data-type="add" data-url="admin/content/add.html" data-group="0" data-id="27" data-icon="larry-fabu1">发布文章</a>
                            </div>
                            <div class="layui-inline">
                                <a class="layui-btn layui-btn-danger layui-btn-normal delAll_btn  layui-btn-sm"  data-type="del" data-id="article">批量删除</a>
                            </div>
                        </div>
                        <form action="{:url('index')}" method="post" class="layui-form  layui-inline">
                            <div class="larryms-search-box">
                                <div class="layui-input-inline">
                                    <input type="text" name="keywords" id="keywords" class="layui-input searchVal layui-inline larry-input" placeholder="请输入骑士姓名" onkeypress="if(event.keyCode==13) {searchBtn.click();return false;}" autocomplete="off">
                                </div>
                                <a class="layui-btn larryms-search" id="searchBtn" data-type="reload">搜索</a>
                            </div>
                        </form>
                        {{--<div class="layui-hide-xs layui-inline">--}}
                            {{--<div class="layui-inline">--}}
                                {{--<a class="layui-btn layui-btn-normal addNews_btn"  data-type="add" data-url="admin/content/add.html" data-group="0" data-id="27" data-icon="larry-fabu1">发布文章</a>--}}
                            {{--</div>--}}
                            {{--<div class="layui-inline">--}}
                                {{--<a class="layui-btn layui-btn-danger layui-btn-normal delAll_btn"  data-type="del" data-id="article">批量删除</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </blockquote>
                    <table id="knightApply" lay-filter="knightApply" class="larryms-table-id" data-url="{{ url('/admin/knight/applyList') }}"></table>
                </div>
            </div>
        </div>
    </div>
    <!--状态-->
    <script type="text/html" id="status">
        @{{#  if(d.kaStatus == "0"){ }}
        <span class="larryms-status-red">申请中</span>
        @{{#  } else if(d.kaStatus == "1"){ }}
        <span class="larryms-status-green">审核通过</span>
        @{{#  } else if(d.kaStatus == "2"){ }}
        <span class="larryms-status-blue">审核不通过</span>
        @{{#  } else { }}
        <span class="larryms-status-gray">封号</span>
        @{{#  } }}
    </script>
    <!--操作-->
    <script type="text/html" id="listBar">
        @{{#  if(d.kaStatus == "0"){ }}
        <a class="layui-btn layui-btn-xs"  data-url="/admin/knight/verify" lay-event="verify">审核</a>
        <a class="layui-btn layui-btn-xs layui-btn-danger" data-url="/admin/knight/refuse" data-id="@{{ d.kaId }}" lay-event="refuse">拒绝</a>
        @{{#  } }}
    </script>
    <!-- 加载js文件-->
<script type="text/javascript" src="{{ URL::asset('assets/larryms/layui/layui.js') }}"></script>
    <script type="text/javascript">

    layui.cache.page = 'knightApply';
    layui.config({
        version: "2.0.8",
        base: '/assets/larryms/',
        identified: 'knightlist'
    }).extend({
        larry: '/js/base'
    }).use('larry');
    </script>
</body>

</html>