<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>用户设置</title>
  <meta name="renderer" content="webkit">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
  <meta name="Author" content="larry" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">	
	<meta name="apple-mobile-web-app-capable" content="yes">	
	<meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="Shortcut Icon" href="{{ URL::asset('favicon.ico') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/layui/css/layui.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/base.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/admin.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/article.css') }}" media="all">

</head>
<body class="larryms-system article">
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<div class="layui-fluid larryms-article">
      <div class="layui-tab-content larryms-panel-body">
           <form action="{{url('/admin/user/add')}}"  width800 class="layui-form" method="post" id="articleAddBox">
                <!--<div class="layui-form-item">
                   <label class="layui-form-label">所在城市：</label>
                   <div class="layui-input-inline">
                       <select name="crId" lay-verify="required" lay-search="" lay-filter="cate">
                           <option value="">直接选择或搜索选择</option>
                       </select>
                   </div>
                   <div class="layui-form-mid layui-word-aux"> 【必填】</div>
                </div>-->
                <input type="hidden" name="uCode" value="{{ $userInfo['uCode'] }}">
                <div class="layui-form-item">
                    <label class="layui-form-label">微信昵称：</label>
                    <div class="layui-input-inline doc-keywords">
                        <input type="text" lay-verify='required' autocomplete="off" value="{{ $userInfo['uName'] }}" class="layui-input larry-input" style="background: #DDDDDD;" readonly="readonly">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">注册姓名：</label>
                    <div class="layui-input-inline doc-keywords">
                        <input type="text" lay-verify='required' autocomplete="off" value="{{ isset($userInfo['addition'])?$userInfo['addition']['uName']:'未注册' }}" class="layui-input larry-input" style="background: #DDDDDD;" readonly="readonly">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">注册学校：</label>
                    <div class="layui-input-inline doc-keywords">
                    	@if(isset($userInfo['addition']))
                        <select name="csId" lay-verify="required" lay-search="" lay-filter="cate">
                           <option value="">直接选择或搜索选择</option>
                           @foreach($schoolList as $school)
                           <option value="{{$school['csId']}}" @if($school['csId'] == $userInfo['addition']['uCustomInfo']['csId']) selected @endif >{{$school['csName']}}</option>
                           @endforeach
                        </select>
                        @else
                        	<input type="text" name="csId" lay-verify='required' autocomplete="off" value="未注册" class="layui-input larry-input" style="background: #DDDDDD;" readonly="readonly">
                        @endif
                    </div>
                </div>
                <div class="layui-form-item submit">
                     <div class="layui-btn" lay-submit lay-filter="add_content">保存</div>
                     <!--<div class="layui-btn" id="resetAll">重置</div>-->
                </div>
           </form>
      </div>
</div>

<!-- 加载js文件-->
<script type="text/javascript" src="{{ URL::asset('assets/larryms/layui/layui.js') }}"></script>
<script type="text/javascript">
layui.cache.page = 'school';
layui.config({
    version: "2.0.8",
    base: '/assets/larryms/',
    identified: 'list'
}).extend({
    larry: '/js/base'
}).use(['larry','form','ueditor','upload','laydate'],function(){
    var $ = layui.$,
        larry = layui.larry,
        form = layui.form,
        larryms = layui.larryms,
        upload = layui.upload,
        laydate = layui.laydate,
        neditor = layui.neditor;
    
   var $html = $('.htmlBody');
   if($html.length>1){
      $html.each(function(){
           var curId = $(this).attr('id'),
               $container = 'ue'+curId;
          $container = UE.getEditor(curId,{initialFrameWidth:null});
      });
   }else if($html.length==1){
      var id = $html.attr('id');
      var ue = UE.getEditor(id,{initialFrameWidth:null});
        ue.ready(function(){
            ue.setHeight(350);
        });
   }
   
   var litpicPost = $('#larry-litpic').data('url');
   var uploadInst = upload.render({
        elem:'#larry-litpic',
        url:litpicPost,
        data:{thumb:true},
        done:function(res){
           //上传成功
                if(res.code>0){
                    $('#litpicPath').val(res.url);
                    $('#litpicPath').siblings('.larryms-img-view').append("<img src='"+res.url+"'>");
                }else{
                    //如果上传失败
                    return layer.msg('上传失败');
                }
        },
        error:function(){

        }
   });
   

   var ids='',upImgUrl='',id='';
   $('.larryms-imgpath-upload').on('click',function(){
        upImgUrl = $(this).data('url');
        ids=$(this);
        id=$(this).siblings('input').data('name');
   });
   if($('.larryms-imgpath-upload').length>0){
       $('.larryms-imgpath-upload').trigger('click');
   }
   var upindex = upload.render({
              elem:ids,
              url:upImgUrl,
              method:'post',
              accept:'file',
              done:function(res){
                  //上传成功
                  if(res.code>0){
                      $('input[name='+id+']').val(res.url);
                      $('input[name='+id+']').siblings('.larryms-img-view').append("<img src='"+res.url+"'>");
                  }else{
                      //如果上传失败
                       return layer.msg(res.msg);
                  }
              },
              error:function(){
                
              }
        });
   // form.on('select(cate)',function(data){
   //       $.ajax({
   //         type:"post",
   //         dataType:'json',
   //         data:{cate_id:data.value},
   //         url:"{:url('getModelId')}",
   //         success:function(res){
   //          // $('#model_id').val(res.mid);
   //          location.href = "{:url('add')}?cate_id="+data.value+"&mid="+res.mid;
   //         }
   //       })
   //  });
  
   laydate.render({
       elem:'#larrymsdate',
       value: new Date(),
       isInitValue: true,
       type: 'datetime'
   });
   form.on('submit(add_content)',function(data){
       $.post(data.form.action,data.field,function(res){
           if(res.code == '0') {
               larryms.alert(res.msg, function () {
                   //跳转到我发布的文档 或 继续添加文档
                   // $('#articleAddBox')[0].reset();
                   // $('#articleAddBox .larryms-img-view').empty();
                   location.reload();
               });
           }else{
               larryms.msg(res.msg);
           }

       });

       return false;
   });
});
</script>
</body>
</html>

        
      
