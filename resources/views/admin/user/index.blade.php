<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>所有文章列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="Author" content="larry" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="Shortcut Icon" href="{{ URL::asset('favicon.ico') }}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/layui/css/layui.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/base.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/admin.css') }}" media="all">
</head>

<body class="">
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <div class="layui-fluid">
        <div class="larry-container">
            <div class="layui-row layui-col-space15 larryms-data-top">
                <div class="layui-col-xs12 layui-col-sm12 layui-col-md12 layui-col-lg12">
                    <blockquote class="layui-elem-quote quoteBox" id="articleBtn">
                        <div class="layui-show-xs layui-hide-sm layui-hide-md layui-hide-lg" style="padding-bottom: 12px;">
                            <div class="layui-inline">
                                <a class="layui-btn layui-btn-normal addNews_btn layui-btn-sm" style="margin-right: 10px;"  data-type="add" data-url="admin/content/add.html" data-group="0" data-id="27" data-icon="larry-fabu1">发布文章</a>
                            </div>
                            <div class="layui-inline">
                                <a class="layui-btn layui-btn-danger layui-btn-normal delAll_btn  layui-btn-sm"  data-type="del" data-id="article">批量删除</a>
                            </div>
                        </div>
                        <form action="{:url('index')}" method="post" class="layui-form  layui-inline">
                            <div class="larryms-search-box">
                                <div class="layui-input-inline">
                                    <input type="text" name="keywords" id="keywords" class="layui-input searchVal layui-inline larry-input" placeholder="请输入用户姓名" onkeypress="if(event.keyCode==13) {searchBtn.click();return false;}" autocomplete="off">
                                </div>
                                <div class="layui-input-inline">
                                    <select name="is_manager" id="is_manager" lay-verify="required" lay-search="" lay-filter="cate">
                                        <option value="" >是否校园经理</option>
                                        <option value="0" >否</option>
                                        <option value="1" >是</option>
                                    </select>
                                </div>
                                <a class="layui-btn larryms-search" id="searchBtn" data-type="reload">搜索</a>
                            </div>
                        </form>
                        {{--<div class="layui-hide-xs layui-inline">--}}
                            {{--<div class="layui-inline">--}}
                                {{--<a class="layui-btn layui-btn-normal addNews_btn"  data-type="add" data-url="admin/content/add.html" data-group="0" data-id="27" data-icon="larry-fabu1">发布文章</a>--}}
                            {{--</div>--}}
                            {{--<div class="layui-inline">--}}
                                {{--<a class="layui-btn layui-btn-danger layui-btn-normal delAll_btn"  data-type="del" data-id="article">批量删除</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </blockquote>
                    <form class="layui-form">
                    <table id="tbl_user" lay-filter="tbl_user" class="larryms-table-id" data-url="{{ url('/admin/user/index') }}"></table>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="config" data-url_auto="{{url('/admin/knight/autoTask')}}"></div>
    <!--状态-->
    <script type="text/html" id="status">
        @{{#  if(d.kStatus == "0"){ }}
        <span class="larryms-status-red">申请中</span>
        @{{#  } else if(d.kStatus == "1"){ }}
        <span class="larryms-status-green">审核通过</span>
        @{{#  } else if(d.kStatus == "2"){ }}
        <span class="larryms-status-blue">审核不通过</span>
        @{{#  } else { }}
        <span class="larryms-status-gray">封@{{d.kStatus}}号</span>
        @{{#  } }}
    </script>
    <!--是否自动接单-->
    <script type="text/html" id="kAutoTask">
        @{{#  if(d.kAutoTask == "0"){ }}
        <input type="checkbox" name="flag" data-id="@{{ d.kId }}" lay-filter="flag" value="@{{ d.kId }}" lay-skin="switch" lay-text="是|否">
        @{{#  } else if(d.kAutoTask == "1"){ }}
        <input type="checkbox" name="flag" data-id="@{{ d.kId }}" lay-filter="flag" value="@{{ d.kId }}" lay-skin="switch" lay-text="是|否" checked>
        @{{#  } }}
    </script>
    <!--操作-->
    <script type="text/html" id="listBar">
		<a class="layui-btn layui-btn-xs" lay-event="edit" data-title="编辑" data-url="add?uCode=@{{ d.uCode }}" data-id="@{{ d.uId }}" data-group="0" data-icon="larry-peizhiguanli">编辑</a>
        @{{#  if(d.isManager == "0"){ }}
        <a class="layui-btn layui-btn-normal layui-btn-xs" data-url="/admin/user/setManager" lay-event="set" data-id="@{{ d.uId }}" data-uschool="@{{ d.uSchool }}">设校经理</a>
        @{{#  } }}
    </script>
    <!-- 加载js文件-->
<script type="text/javascript" src="{{ URL::asset('assets/larryms/layui/layui.js') }}"></script>
    <script type="text/javascript">

    layui.cache.page = 'user';
    layui.config({
        version: "2.0.8",
        base: '/assets/larryms/',
        identified: 'knightlist'
    }).extend({
        larry: '/js/base'
    }).use('larry');
    </script>
</body>

</html>