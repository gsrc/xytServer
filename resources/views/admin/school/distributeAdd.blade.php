<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>系统参数分组编辑</title>
  	<meta name="renderer" content="webkit">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
  	<meta name="Author" content="larry" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">	
	<meta name="apple-mobile-web-app-capable" content="yes">	
	<meta name="format-detection" content="telephone=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="Shortcut Icon" href="{{ URL::asset('favicon.ico') }}" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/layui/css/layui.css') }}" media="all">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/base.css') }}" media="all">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/larryms/css/admin/admin.css') }}" media="all">
</head>
<body class="larryms-system larryms-system-add">
<div class="layui-fluid">
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
  <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
  <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
	<div class="layui-row larryms-panel auth-user-add">
		<form action="{{url('/admin/school/distributeAdd')}}" class="layui-form" method="post" id="box_form">
                <input type="hidden" name="csId" value="{{$request->id}}">

            <input type="hidden" name="stId" value="{{$request->stId}}">

            	<div class="layui-form-item">
            	    <label class="layui-form-label">请填写配送点</label>
            	    <div class="layui-input-block larry-textarea">
            	         <textarea name="stName" placeholder="请填写配送点" class="layui-textarea" lay-verify="required" value=''  autocomplete="off">{{$stName}}</textarea>
            	    </div>
            	</div>
           		<div class="larryms-layer-btn" style="text-align: center">
           		      <a class="layui-btn layui-btn layui-btn-sm left" lay-submit="" lay-filter="btn_submit" id="confgroupAdd">确定</a>
           		      <a class="layui-btn layui-btn layui-btn-sm  layui-btn-danger" id="resetAdd">重置</a>
           		      <a class="layui-btn layui-btn layui-btn-sm  layui-btn-danger" id="closeAdd">关闭</a>
           		</div>

		</form>
	</div>
</div>
<!-- 加载js文件-->
<script type="text/javascript" src="{{ URL::asset('assets/larryms/layui/layui.js') }}"></script>
<script type="text/javascript">

    layui.cache.page = 'distribute';
layui.config({
   version:"2.0.8",
   base:'/assets/larryms/',  //实际使用时，建议改成绝对路径
   identified:'list',
   layertype: 2, //iframe内层弹窗类型不显示返回首页
   rightMenu: false // false关闭，设置为custom时使用自定义，不使用默认menu
}).extend({
    larry:'/js/base'
}).use(['larry','table'],function(){
   var $ = layui.$,
      larry = layui.larry,
      table = layui.talbe,
      larryms = layui.larryms,
      form = layui.form;
  var curIfr = parent.layer.getFrameIndex(window.name);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  $('#resetAdd').on('click',function(){
      $('#box_form')[0].reset();
  });
  $('#closeAdd').on('click',function(){
     parent.layer.close(curIfr);   
  });

form.on('switch(switchTest)',function(data){
    // if(data.elem.checked){
    //     $("input[name='is_extend']").val('1');
    // }else{
    //     $("input[name='is_extend']").val('0');
    // }
    console.log(data.value);
});
  form.on('submit(btn_submit)',function(data){

      $.post(data.form.action,data.field,function(res){
          //console.log(res)
          //return false;
          if(res.code == '0'){
              larryms.msg(res.msg);
              parent.layui.table.reload('distribute',{});
              parent.layer.close(curIfr);
              // $('#box_form')[0].reset();继续添加注销上一段
              
          }else{
              larryms.msg(res.msg);
          }
      });
      return false;
  });
        
});
</script>
</body>
</html>