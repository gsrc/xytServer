<style>
    .img{
        max-width: 200px;
    }

</style>

<table border="1">
  <tr>
    <th>商家</th>
    <th>订单号</th>
    <th>用户昵称</th>
    <th>电话</th>
    <th>金额</th>
    <th>备注</th>
    <th>图片备注</th>
    <th>上传图片</th>
    <th>下单时间</th>
  </tr>
  @foreach ($listData as $info)
  <tr>
      <td>{{$info->shop_name}}</td>
      <td>{{$info->order_sn}}</td>
      <td>{{$info->user_name}}</td>
      <td>{{$info->phone}}</td>
      <td>{{$info->price_pay}}</td>
      <td>{{$info->remark}}</td>
      <td>{{$info->pgDescribe}}</td>
      <td>
          @foreach ($info->picList as $picInfo)
            <img class="img" src="{{$picInfo->pUrl}}" />
          @endforeach
      </td>
      <td>{{$info->create_at}}</td>
  </tr>
  @endforeach

</table>
