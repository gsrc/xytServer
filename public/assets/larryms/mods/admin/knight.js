layui.define(['jquery', 'table', 'larryms', 'common', 'laytpl', 'form'], function(exports) {
	var $ = layui.$,
		table = layui.table,
		common = layui.common,
        laytpl = layui.laytpl,
		form = layui.form,
		larryms = layui.larryms;
	var config = $("#config").data();

	//时间戳的处理
	layui.laytpl.toDateString = function(d, format) {
		var date = new Date(d || new Date()),
			ymd = [
				this.digit(date.getFullYear(), 4), this.digit(date.getMonth() + 1), this.digit(date.getDate())
			],
			hms = [
				this.digit(date.getHours()), this.digit(date.getMinutes()), this.digit(date.getSeconds())
			];

		format = format || 'yyyy-MM-dd HH:mm:ss';

		return format.replace(/yyyy/g, ymd[0])
			.replace(/MM/g, ymd[1])
			.replace(/dd/g, ymd[2])
			.replace(/HH/g, hms[0])
			.replace(/mm/g, hms[1])
			.replace(/ss/g, hms[2]);
	};

	//数字前置补零
	layui.laytpl.digit = function(num, length, end) {
		var str = '';
		num = String(num);
		length = length || 2;
		for (var i = num.length; i < length; i++) {
			str += '0';
		}
		return num < Math.pow(10, length) ? str + (num | 0) : num;
	};

	if (layui.cache.identified == 'knightlist') {
		//搜索【此功能需要后台配合，所以暂时没有动态效果演示】
		$("#searchBtn").on("click", function() {
			if ($(".searchVal").val() != '') {
				table.reload("knight", {
					page: {
						curr: 1 //重新从第 1 页开始
					},
					where: {
						key: $(".searchVal").val() //搜索的关键字
					}
				})
			} else {
				larryms.msg("请输入搜索的内容");
			}
		});

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
		var mUrl = $('#knight').data('url'),
			mIns = table.render({
				elem: '#knight',
				id: "knight",
				cellMinWidth: 95,
				url: mUrl,
				method: 'post',
                //where:{},
				height: "full-155",
				page: true,
				limits: [15, 30, 45, 60, 75, 90, 105, 120],
				limit: 15,
				cols: [
					[{
						type: "checkbox",
						fixed: 'left',
						width: 40
					}, {
                        field: 'uName',
                        title: '骑士姓名',
                        minWidth: 150,
                        align: 'left'
                    }, {
                        field: 'uMobile',
                        title: '电话',
                        minWidth: 130,
                        align: 'left'
                    }, {
                        field: 'klName',
                        title: '用户等级',
                        width: 120,
                        align: 'center',
                    }, {
                        field: 'csName',
                        title: '所属学校',
                        width: 120,
                        align: 'center',
                    }, {
                        field: 'kAutoTask',
                        title: '是否自动接单',
                        width: 150,
                        templet: '#kAutoTask'
                    }, {
                        field: 'kGetNum',
                        title: '接单数',
                        width: 150,
                        align: 'center'
                    }, {
						field: 'kDoneNum',
						title: '完成单数',
						width: 150,
						align: 'center'
					},
					{
                        field: 'kCreditNum',
                        title: '信用值',
                        width: 170,
                        align: 'center'
                    }, {
                        field: 'kStatus',
                        title: '状态',
                        width: 100,
                        align: 'center',
                        templet: "#status"
                    },
                        {
                            field: 'kCheckTime',
                            title: '审核时间',
                            width: 170,
                            align: 'center'
                        }, {
						field: 'createTime',
						title: '创建时间',
						width: 170,
						align: 'center',
					}, {
						title: '操作',
						width: 200,
						templet: '#listBar',
						fixed: "right",
						align: "center"
					}]
				]
			});

	}
	//按钮组监听
	$('#articleBtn .layui-btn').on('click', function() {
		var type = $(this).data('type'),
			url = $(this).data('url'),
			tit = $(this).children('cite').text();
		if (type == 'add') {
			var data = {
				href: $(this).data('url'),
				id: $(this).data('id'),
				font: 'larry-icon',
				icon: $(this).data('icon'),
				group: $(this).data('group'),
				title: '文章发布',
				addType: 'iframe'
			};
			active[type].call(this, data);
		} else {
			active[type] ? active[type].call(this, url, tit) : '';
		}

	});
	var active = {
		add: function(data) {

			common.tab.addTab(data, 'iframe');
		},
		del: function() {
			//批量删除
			var tableID = $(this).data('id'),
				checkStatus = table.checkStatus(tableID),
				data = checkStatus.data,
				newsId = [];

			var url = $(this).data('url');
			if (data.length > 0) {
				for (var i in data) {
					newsId.push(data[i].id);
				}
				if (newsId.length > 0) {
					var ids = {
						"id": newsId
					};
					larryms.confirm('你确定要执行批量删除吗？', {
						icon: 3,
						title: '批量删除提示！'
					}, function() {
						$.post(url, ids, function(res) {
							// if (res.code == 200) {
							// 	larryms.msg(res.msg);
							// 	table.reload(tableID, {
							// 		page: {
							// 			curr: 1
							// 		}
							// 	});
							// } else {
							// 	larryms.msg(res.msg);
							// }
							larryms.msg('已执行到删除，实际项目中请填写后端处理url即可');
						});
					});
				}
			} else {
				larryms.msg('请至少选择一项，进行删除操作');
			}
		}
	};
	 var pageTableID = $('.larryms-table-id').attr('id');
	// 监听工具条
	table.on('tool(' + pageTableID + ')', function(obj) {
		var data = obj.data;
		if (obj.event == 'edit') {
			if (layui.cache.identified == 'index') {
				data.aid = data.id;
			}
			var url = $(this).data('url') + '?aid=' + data.aid + '&cid=' + data.cid;

			location.href = url;
		}else if(obj.event == 'del'){
			var url = $(this).data('url');
			larryms.confirm('你确定要删除该条数据吗？', {
				icon: 3,
				title: '删除提示'
			}, function() {
				var ids = {
					"id": data.id
				};
				$.post(url, ids, function(res) {
					// if (res.code == 200) {
					// 	larryms.msg(res.msg);
					// 	table.reload(pageTableID, {});
					// } else {
					// 	larryms.msg(res.msg);
					// }
					larryms.msg('已执行到删除，实际项目中请填写后端处理url即可');
				});
			});
		}else if(obj.event == 'look'){
			larryms.alert('预览操作，可自定义再当前页面或新窗口打开当前选择的文档地址进行预览');
		}else if(obj.event == 'verify'){
            var url = $(this).data('url');
            larryms.confirm('你确定要通过审核吗？', {
                icon: 1,
                title: '审核提示'
            }, function() {
                var ids = {
                    "id": data.kId
                };
                $.post(url, ids, function(res) {
                    larryms.msg(res.msg);
                    if (res.code == '0') {
                        larryms.msg(res.msg);
                        table.reload(pageTableID, {});
                    }
                }, "json");
            });
        }
	});

    form.on('switch(flag)', function(data){
        //console.log(data.elem.data()); //得到checkbox原始DOM对象
        //console.log(data.elem.checked); //开关是否开启，true或者false
        //console.log(data.value); //开关value值，也可以通过data.elem.value得到
        //console.log(data.othis); //得到美化后的DOM对象
		var params = {};
		params['id'] = data.value;
		if(data.elem.checked){
            params['status'] = 1;
		}else{
            params['status'] = 2;
        }
        $.post(config.url_auto, params, function(res) {
            //console.log(res);
        }, "json");
    });

    exports('knight', {});
});