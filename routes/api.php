<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::any('/login/test', 'Api\LoginController@test');
//
//Route::any('/login/test2', 'Api\LoginController@test2');

Route::any('v1/account/admin', 'Api\V1\AccountController@admin');

Route::any('v1/account/user', 'Api\V1\AccountController@user');


Route::any('v1/express/publish', 'Api\V1\ExpressController@publish');




Route::any('/comment/create', 'Api\CommentController@create');

Route::any('/comment/like', 'Api\CommentController@like');

Route::any('/comment/list', 'Api\CommentController@list');

Route::any('/comment/commentList', 'Api\CommentController@commentList');

Route::any('/comment/tagList', 'Api\CommentController@tagList');

Route::group(['middleware'=>'accessAuth'],function(){

    Route::any('/login/test', 'Api\LoginController@test');

    Route::any('/login/test2', 'Api\LoginController@test2');
});

Route::group(['prefix'=>'didi'],function(){

    Route::any('/user/carSet', 'Didi\UserController@carSet');
    Route::any('/user/carInfo', 'Didi\UserController@carInfo');
    Route::any('/user/checkInviteCode', 'Didi\UserController@checkInviteCode');
    Route::any('/user/beInvited', 'Didi\UserController@beInvited');
    Route::any('/user/billList', 'Didi\UserController@billList');

});

