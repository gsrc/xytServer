<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'admin', function () {
    return view('account/admin');
}]);

Route::any('/test', 'TestController@index');

Route::group(['middleware'=>'admin', 'prefix'=>'admin'],function(){


    Route::any('/index', 'Admin\IndexController@index');
    Route::any('/console', 'Admin\IndexController@console');
    Route::any('/logout', 'Admin\IndexController@logout');

    // 骑士列表
    Route::any('/knight/index', 'Admin\KnightController@index');
    // 申请列表
    Route::any('/knight/applyList', 'Admin\KnightController@applyList');
    // 骑士审核
    Route::any('/knight/verify', 'Admin\KnightController@verify');
    // 骑士拒绝
    Route::any('/knight/refuse', 'Admin\KnightController@refuse');
    // 骑士自动接单任务开关
    Route::any('/knight/autoTask', 'Admin\KnightController@autoTask');




    // 学校列表
    Route::any('/school/index', 'Admin\SchoolController@index');
    // 学校添加
    Route::any('/school/add/{id?}/{name?}', 'Admin\SchoolController@add');
    // 学校配送点
    Route::any('/school/distribute/{id}/{name?}', 'Admin\SchoolController@distribute');
    // 学校配送点添加/编辑
    Route::any('/school/distributeAdd/{id?}/{stId?}', 'Admin\SchoolController@distributeAdd');
    // 学校配送点删除
    Route::any('/school/distributeDel/{id}', 'Admin\SchoolController@distributeDel');
    // 站点列表
    Route::any('/school/site/{csId}/{type?}/{name?}', 'Admin\SchoolController@site');
    // 站点添加/编辑
    Route::any('/school/siteAdd/{csId?}/{type?}/{ssId?}', 'Admin\SchoolController@siteAdd');
    // 站点删除
    Route::any('/school/siteDel/{id}', 'Admin\SchoolController@siteDel');



    // 用户列表
    Route::any('/user/index', 'Admin\UserController@index');
    // 用户编辑
    Route::any('/user/add', 'Admin\UserController@add');
    // 用户提现
    Route::any('/user/withdraw', 'Admin\UserController@withdraw');
    // 提现审核
    Route::any('/user/withdrawAgree', 'Admin\UserController@withdrawAgree');
    // 提现拒绝
    Route::any('/user/withdrawRefuse', 'Admin\UserController@withdrawRefuse');
    // 用户信息
    Route::any('/user/userinfo', 'Admin\UserController@userinfo');
    // 设置校园经理
    Route::any('/user/setManager', 'Admin\UserController@setManager');

    // 订单列表
    Route::any('/order/index', 'Admin\OrderController@index');

    // 统计
    Route::any('/statistics/index', 'Admin\StatisticsController@index');
    Route::any('/statistics/user', 'Admin\StatisticsController@user');

    // 快递
    Route::any('/express/index', 'Admin\ExpressController@index');
    Route::any('/express/add/{id?}', 'Admin\ExpressController@add');
    Route::any('/express/del/{id}', 'Admin\ExpressController@del');
});

// 认证接口
Route::group(['middleware'=>'apiRequest', 'prefix'=>'api'],function(){

    // 微信小程序code登录
    Route::any('/wxCodeLogin', 'Auth\IndexController@wxCodeLogin')->name('wxCodeLogin');
    // 用户更新订单状态
    Route::post('/affirmOrder', 'Auth\IndexController@affirmOrder')->name('affirmOrder');
    // 退款
    Route::post('/refundMoney', 'Auth\IndexController@refundMoney')->name('refundMoney');
    // 设置用户附加信息
    Route::post('/setUserAddition', 'Auth\IndexController@setUserAddition')->name('setUserAddition');
    // 获取学校下拉框
    Route::get('/getSchoolList', 'Auth\IndexController@getSchoolList')->name('getSchoolList');
    // 解析小程序密文
    Route::post('/decodeWxMiniData', 'Auth\IndexController@decodeWxMiniData')->name('decodeWxMiniData');
    // 发送短信验证码
    Route::post('/sendSMSCode', 'Auth\IndexController@sendSMSCode')->name('sendSMSCode');
    // 获取七牛云上传token
    Route::get('/getQiniuTokenInfo/{token}', 'Auth\IndexController@getQiniuTokenInfo')->name('getQiniuTokenInfo');
    // 获取首页所需信息
    Route::get('/getIndexNeedInfo/{csId}', 'Auth\IndexController@getIndexNeedInfo')->name('getIndexNeedInfo');
    // 获取用户公众号渠道二维码
    Route::get('/wxQrCode', 'Auth\IndexController@wxQrCode')->name('wxQrCode');
    // 店铺回调
    Route::post('/shopCallback', 'Auth\IndexController@shopCallback')->name('shopCallback');
    // 添加公众号分组
    Route::get('/addMerchant/{wpgCode}', 'Auth\IndexController@addMerchant')->name('addMerchant');

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 申请骑士
    Route::any('/apply', 'Knight\IndexController@apply')->name('apply');
    // 获取待接任务订单列表
    Route::any('/getTaskList', 'Knight\IndexController@getTaskList')->name('getTaskList');
    // 获取报名信息
    Route::get('/getApplyInfo', 'Knight\IndexController@getApplyInfo')->name('getApplyInfo');
    // 获取骑士待接任务订单列表
    Route::get('/getToDoOrderList/{token}', 'Knight\IndexController@getToDoOrderList')->name('getToDoOrderList');
    // 获取骑士任务订单列表
    Route::get('/getToDoOrderSwichList/{token}', 'Knight\IndexController@getToDoOrderSwichList')->name('getToDoOrderSwichList');
    // 获取骑士接单历史列表
    Route::get('/getOrderHistory/{token}', 'Knight\IndexController@getOrderHistory')->name('getOrderHistory');
    // 骑士更改订单状态
    Route::post('/knightChangeOrderStatus', 'Knight\IndexController@knightChangeOrderStatus')->name('knightChangeOrderStatus');
    // 获取接单学校下拉框
    Route::get('/getKnightSchoolList/{token}', 'Knight\IndexController@getKnightSchoolList')->name('getKnightSchoolList');
    // 提交指定接单地址
    Route::post('/assignAddress', 'Knight\IndexController@assignAddress')->name('assignAddress');
    // 获取可选时间段列表
    Route::get('/getScheduleList/{token}', 'Knight\IndexController@getScheduleList')->name('getScheduleList');
    // 提交任务时间段
    Route::post('/setHour', 'Knight\IndexController@setHour')->name('setHour');
    // 每日自动生成骑士任务
    Route::get('/autoPublishTask', 'Knight\IndexController@autoPublishTask')->name('autoPublishTask');
    // 获取骑士未完成订单数
    Route::get('/getToDoOrderNum/{token}', 'Knight\IndexController@getToDoOrderNum')->name('getToDoOrderNum');
    // 获取骑士页所需信息
    Route::get('/getKnightPageInfo/{token}', 'Knight\IndexController@getKnightPageInfo')->name('getKnightPageInfo');
    // 设置骑士信息
    Route::post('/setKnightInfo', 'Knight\IndexController@setKnightInfo')->name('setKnightInfo');
    // 获取学校地点列表
    Route::get('/getSchoolSitList/{token}/{csId}', 'Knight\IndexController@getSchoolSitList')->name('getSchoolSitList');
    // 获取在线服务骑士数
    Route::get('/getOnlineNum/{csId}/{showDetail?}', 'Knight\IndexController@getOnlineNum')->name('getOnlineNum');



    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 获取地址下拉框
    Route::get('/getRangeList/{rangeType}/{parentId}', 'Order\IndexController@getRangeList')->name('getRangeList');
    // 添加收货地址
    Route::post('/addReceiveAddress', 'Order\IndexController@addReceiveAddress')->name('addReceiveAddress');
    // 添加收货地址
    Route::get('/getReceiveAddressList/{token}/{orderType?}', 'Order\IndexController@getReceiveAddressList')->name('getReceiveAddressList');
    // 获取默认收货地址
    Route::get('/getDefaultReceiveAddress/{token}', 'Order\IndexController@getDefaultReceiveAddress')->name('getDefaultReceiveAddress');
    // 获取收货地址
    Route::get('/getReceiveAddress/{aaId}/{token}', 'Order\IndexController@getReceiveAddress')->name('getReceiveAddress');
    // 获取取货地址列表
    Route::get('/getTakeAddressList/{csId}', 'Order\IndexController@getTakeAddressList')->name('getTakeAddressList');
    // 获取取货地址
    Route::get('/getTakeAddress/{stId}', 'Order\IndexController@getTakeAddress')->name('getTakeAddress');
    // 添加订单
    Route::post('/addOrder', 'Order\IndexController@addOrder')->name('addOrder');
    // 获取用户订单列表
    Route::get('/getUserOrderList/{orderType}/{token}', 'Order\IndexController@getUserOrderList')->name('getUserOrderList');
    // 获取创建订单需要的页面信息
    Route::get('/getCreateOrderInfo/{token}/{aaId?}/{orderType?}/{takeAaId?}', 'Order\IndexController@getCreateOrderInfo')->name('getCreateOrderInfo');
    // 删除收货地址信息
    Route::post('/delReceiveAddress', 'Order\IndexController@delReceiveAddress')->name('delReceiveAddress');
    // 获取订单默认金额
    Route::get('/getDefaultPay/{csId}/{stId}/{ssId}/{size}', 'Order\IndexController@getDefaultPay')->name('getDefaultPay');
    // 微信支付回调
    Route::post('/payCallback', 'Order\IndexController@payCallback')->name('payCallback');
    // 获取订单详情
    Route::get('/getOrderInfo/{token}/{aaId}', 'Order\IndexController@getOrderInfo')->name('getOrderInfo');
    // 支付未完成支付的订单
    Route::post('/payOrder', 'Order\IndexController@payOrder')->name('payOrder');
    // 自动派单
    Route::get('/autoSendOrder/{aoId}', 'Order\IndexController@autoSendOrder')->name('autoSendOrder');
    // 派单任务
    Route::get('/autoSendOrderTask', 'Order\IndexController@autoSendOrderTask')->name('autoSendOrderTask');
    // 通过取货地点获取快递公司列表
    Route::get('/getTakeExpress/{stId}', 'Order\IndexController@getTakeExpress')->name('getTakeExpress');
    // 获取价格配置
    Route::get('/getPriceConfig/{csId}/{type}', 'Order\IndexController@getPriceConfig')->name('getPriceConfig');
    // 获取外卖跑腿价格配置
    Route::get('/getTakeOutPriceConfig/{csName}', 'Order\IndexController@getTakeOutPriceConfig')->name('getTakeOutPriceConfig');
    // 提交推荐人活动
    Route::get('/recommendActivity/{uId}', 'Order\IndexController@recommendActivity')->name('recommendActivity');
    // 获取商品订单信息
    Route::get('/getGoodsOrderInfo/{orderCode}', 'Order\IndexController@getGoodsOrderInfo')->name('getGoodsOrderInfo');
    // 获取商品订单支付信息
    Route::get('/getGoodsPayInfo/{orderCode}', 'Order\IndexController@getGoodsPayInfo')->name('getGoodsPayInfo');
    // 获取商城手机加密字符串
    Route::get('/getMobileToken/{token}', 'Order\IndexController@getMobileToken')->name('getMobileToken');
    // 获取订单状态
    Route::get('/getOrderStatue/{orderCode}', 'Order\IndexController@getOrderStatue')->name('getOrderStatue');
    // 向商家付款
    Route::post('/payToMerchant', 'Order\IndexController@payToMerchant')->name('payToMerchant');
    // 转单
    Route::post('/acceptTurnOrder', 'Order\IndexController@acceptTurnOrder')->name('acceptTurnOrder');

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 用户相关
    Route::any('/suggest', 'Auth\IndexController@suggest')->name('suggest');



    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 获取钱包账户信息
    Route::get('/getAccountInfo/{token}', 'Wallet\IndexController@getAccountInfo')->name('getAccountInfo');
    // 获取钱包流水
    Route::get('/getTradeList/{token}', 'Wallet\IndexController@getTradeList')->name('getTradeList');
    // 提交提现申请
    Route::post('/applyWithdraw', 'Wallet\IndexController@applyWithdraw')->name('applyWithdraw');

    // 上传图集
    Route::post('/addPhotos', 'UploadController@addPhotos')->name('addPhotos');
    // 重置图集
    Route::post('/resetPhotos', 'UploadController@resetPhotos')->name('resetPhotos');
    // 获取图片集列表
    Route::any('/getPhotoGroupList', 'UploadController@getPhotoGroupList')->name('getPhotoGroupList');
    // 商户绑定微信
    Route::get('/shopBindWechat/{shopAccount}/{openid}', 'AccountController@shopBindWechat')->name('shopBindWechat');
    // 骑士绑定微信
    Route::get('/bindALLKnightForWechar/{openid}/{schoolId}/{phone}', 'AccountController@bindALLKnightForWechar')->name('bindALLKnightForWechar');

    // 添加留言
    Route::post('/addComment', 'Comment\IndexController@addComment')->name('addComment');
    // 获取留言列表
    Route::any('/getCommentList', 'Comment\IndexController@getCommentList')->name('getCommentList');

});

Route::any('/admin', 'AccountController@admin');

Route::get('/getYesterdayExpressList', 'Order\IndexController@getYesterdayExpressList')->name('getYesterdayExpressList');
